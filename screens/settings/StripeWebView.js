import React, { useEffect, useState, useContext } from 'react'
import { ActivityIndicator } from 'react-native'
import { WebView } from 'react-native-webview'
import AppContext from '../../context/app-context'
import axios from 'axios'

export default ({ navigation }) => {
	const { user } = React.useContext(AppContext)
	const [isLoading, setIsLoading] = useState(true)
	const [stripeUrl, setStripeUrl] = useState(null)

	useEffect(() => {
		fetchStripeUrlAsync()
	}, [user])

	const fetchStripeUrlAsync = async () => {
		setIsLoading(true)
		const uri = user.stripe_id ? '/courier/stripe/dashboard-url' : '/courier/stripe/connect-onboarding-url'
		try {
			const { data } = await axios.get(uri)
			setStripeUrl(data.url)
		} catch (error) {
			console.log(error.response)
		}
		setIsLoading(false)
	}

	if (isLoading) return <ActivityIndicator style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} />

	return (
		<WebView
			originWhitelist={['*']}
			source={{ uri: stripeUrl }}
			startInLoadingState
			onError={(error) => console.log(error)}
			// renderLoading={() => <ActivityIndicator style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} />}
		/>
	)
}
