import React from 'react'
import { WebView } from 'react-native-webview'

export default () => {
	return (
		<WebView
			originWhitelist={['*']}
			source={{ uri: 'https://tawk.to/chat/5ea6e5db69e9320caac7bd1b/default' }}
			startInLoadingState
			onError={(error) => console.log(error)}
		/>
	)
}
