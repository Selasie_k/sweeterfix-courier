import React, { useState, useContext } from 'react'
import { View, AsyncStorage, Alert } from 'react-native'
import AppContext from '../../context/app-context'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Content, Spinner, ListItem, Left, Right, Button, Text, Form, Item, Input, Label } from 'native-base'
import axios from 'axios'

export default ({ navigation }) => {
	const { user, storeUser } = useContext(AppContext)
	const [currentPassword, setCurrentPassword] = useState(null)
	const [newPassword, setNewPassword] = useState(null)
	const [repeatPassword, setRepeatPassword] = useState()
	const [isLoading, setIsLoading] = useState(false)
	const [errors, setErrors] = useState(null)

	const handleSavePress = () => {
		if (!currentPassword || !newPassword || !repeatPassword) {
			Alert.alert('Incomplete data', 'Please complete the form before proceeding')
			return
		}
		setIsLoading(true)
		axios
			.post('/courier/password-reset', {
				current_password: currentPassword,
				password: newPassword,
				password_confirmation: repeatPassword,
			})
			.then(({ data: { user } }) => {
				storeUser(user)
				setIsLoading(false)
				Alert.alert('Password updated')
			})
			.catch(({ response }) => {
				console.log(response.data)
				setErrors(response.data.errors)
				setIsLoading(false)
				Alert.alert('Error!', 'Something went wrong with your request')
			})
	}

	return (
		<Content>
			<ListItem itemDivider />
			<Form style={{ backgroundColor: 'white' }}>
				<Item>
					<Label>Current password:</Label>
					<Input
						secureTextEntry
						style={{ paddingRight: 16 }}
						value={currentPassword}
						onChangeText={(text) => setCurrentPassword(text)}
						textAlign='right'
					/>
				</Item>
				<Text style={{ color: 'red', fontSize: 13, marginLeft: 16 }}>{errors?.current_password && errors.current_password[0]}</Text>
				{/* <Text style={styles.error}>{errors && errors.email && errors.email[0]}</Text> */}

				<Item>
					<Label>New Password:</Label>
					<Input
						secureTextEntry
						style={{ paddingRight: 16 }}
						value={newPassword}
						onChangeText={(text) => setNewPassword(text)}
						textAlign='right'
					/>
				</Item>
				<Text style={{ color: 'red', fontSize: 13, marginLeft: 16 }}>{errors?.password && errors.password[0]}</Text>

				<Item>
					<Label>Repeat password:</Label>
					<Input
						secureTextEntry
						style={{ paddingRight: 16 }}
						value={repeatPassword}
						onChangeText={(text) => setRepeatPassword(text)}
						textAlign='right'
					/>
				</Item>
				<ListItem itemDivider />
				<ListItem itemDivider />
			</Form>
			<Button dark block disabled={isLoading} onPress={handleSavePress} style={{ margin: 16 }}>
				{isLoading ? <Spinner size='small' color='white' /> : <Text>Done</Text>}
			</Button>
		</Content>
	)
}
