import React from 'react'
import { View, Button, AsyncStorage } from 'react-native'
import AppContext from '../../context/app-context'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Content, List, ListItem, Left, Right, Body, Text, Thumbnail, Separator } from 'native-base'
import { CacheImage } from '../../components/CacheImage'
import { WebView } from 'react-native-webview'
import * as WebBrowser from 'expo-web-browser'
import axios from 'axios'
import AppConfig from '../../config/app'

export default ({ navigation }) => {
	const { setIsSignedIn, user, setUser } = React.useContext(AppContext)

	const handleLogoutPress = async () => {
		AsyncStorage.removeItem('USER').then(() => {
			setUser(null)
			// setIsSignedIn(false)
		})
		await AsyncStorage.removeItem('EXPO_TOKEN')
	}

	const toEditProfileScreen = () => {
		navigation.navigate('EditProfile')
	}

	const openSupportPage = () => {
		navigation.navigate('SupportScreen')
		// WebBrowser.openBrowserAsync('https://tawk.to/chat/5ea6e5db69e9320caac7bd1b/default')
	}

	return (
		<Content>
			<List style={{ backgroundColor: 'white' }}>
				<ListItem itemDivider />
				<ListItem itemDivider />

				<ListItem thumbnail last onPress={toEditProfileScreen}>
					<Left>
						<Thumbnail source={{ uri: user.photo }} />
						{/* <CacheImage style={{ height: 100, width: 100, borderRaduis: 50 }} uri={user.photo} /> */}
					</Left>
					<Body>
						<Text>{user.name}</Text>
						<Text style={{ color: 'gray' }}>{user.phone}</Text>
						{/* <Text style={{ color: 'gray' }}>{user.email}</Text> */}
					</Body>
					<Right>
						<MaterialCommunityIcons name='chevron-right' color='gray' size={20} />
					</Right>
				</ListItem>
				{/* <Separator /> */}

				<ListItem itemDivider />
				<ListItem itemDivider />

				{/* <ListItem icon onPress={toEditProfileScreen}>
					<Body>
						<Text>Name</Text>
					</Body>
					<Right>
						<Text>{user.name}</Text>
						<MaterialCommunityIcons name='chevron-right' color='gray' size={20} />
					</Right>
				</ListItem> */}
				{/* <ListItem icon onPress={toEditProfileScreen}>
					<Body>
						<Text>Phone</Text>
					</Body>
					<Right>
						<Text>{user.phone}</Text>
						<MaterialCommunityIcons name='chevron-right' color='gray' size={20} />
					</Right>
				</ListItem>*/}
				<ListItem icon>
					<Body>
						<Text>Email</Text>
					</Body>
					<Right>
						<Text>{user.email}</Text>
					</Right>
				</ListItem>
				<ListItem last icon onPress={() => navigation.navigate('ChangePassword')}>
					<Body>
						<Text>Password</Text>
						{user.temp_password && <Text style={{ fontSize: 10, color: 'red' }}>Change default password</Text>}
					</Body>
					<Right>
						<Text>************</Text>
						<MaterialCommunityIcons name='chevron-right' color='gray' size={20} />
					</Right>
				</ListItem>
				{/* <ListItem itemDivider>
					<Text style={{ fontSize: 13, color: 'red' }}>*Change default password</Text>
				</ListItem> */}
				<ListItem itemDivider />
				<ListItem itemDivider />
				<ListItem itemDivider>
					<Text style={{ color: 'gray', fontSize: 12 }}>Payout Method</Text>
				</ListItem>
				<ListItem last icon onPress={() => navigation.navigate('StripeWebView')}>
					<Body>
						<Text>Stripe Account</Text>
					</Body>
					<Right>
						<MaterialCommunityIcons name='bank' color='gray' size={20} />
					</Right>
				</ListItem>
				<ListItem itemDivider />
				<ListItem itemDivider>
					<Text style={{ color: 'gray', fontSize: 12 }}>Contact support</Text>
				</ListItem>
				<ListItem last icon onPress={openSupportPage}>
					<Body>
						<Text>Customer Support</Text>
					</Body>
					<Right>
						<MaterialCommunityIcons name='headset' color='gray' size={20} />
					</Right>
				</ListItem>
				<ListItem itemDivider />
				<ListItem itemDivider />
				<ListItem last onPress={handleLogoutPress}>
					<Text style={{ color: 'red' }}>Logout</Text>
				</ListItem>
				<ListItem itemDivider />
				<ListItem itemDivider>
					<Text style={{ color: 'gray', fontSize: 12 }}>App version: {AppConfig.version}</Text>
				</ListItem>
				<ListItem itemDivider />
				<ListItem itemDivider />
			</List>
		</Content>
	)
}
