import React, { useState, useContext, useRef } from 'react'
import { View, TouchableOpacity, Alert, StyleSheet, ActivityIndicator } from 'react-native'
import AppContext from '../../context/app-context'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Content, Spinner, ListItem, Left, Right, Button, Text, Form, Item, Input, Label } from 'native-base'
import axios from 'axios'
import CacheImage from '../../components/CacheImage'
import Modal from 'react-native-modal'
import Layout from '../../constants/Layout'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import ActionSheet from 'react-native-actionsheet'

export default ({ navigation }) => {
	const { user, setUser } = useContext(AppContext)
	const [name, setName] = useState(user.name)
	const [phone, setPhone] = useState(user.phone)
	const [email, setEmail] = useState(user.email)
	const [isLoading, setIsLoading] = useState(false)
	const [imageModalVisible, setImageModalVisible] = useState(false)
	const imageActionSheet = useRef()

	const handleSavePress = () => {
		if (!name || !phone) {
			Alert.alert('Incomplete data', 'Please provide provide your name and phone number')
			return
		}
		setIsLoading(true)
		axios
			.patch('/courier', { name, phone })
			.then(({ data: { user } }) => {
				setUser(user)
				setIsLoading(false)
				Alert.alert('Profile updated')
			})
			.catch(({ response }) => {
				setIsLoading(false)
				Alert.alert('Error!', 'Something went wrong with your request')
			})
	}

	const chooseImageAsync = async () => {
		const { granted } = await Permissions.getAsync(Permissions.CAMERA_ROLL)
		if (!granted) {
			const { granted } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
			if (granted) {
				ImagePicker.launchImageLibraryAsync({
					mediaTypes: ImagePicker.MediaTypeOptions.Images,
					allowsEditing: true,
					aspect: [1, 1],
					quality: 0.5,
					base64: true,
				}).then((image) => !image.cancelled && uploadImageAsync(image))
			} else {
				Alert.alert('Camera roll permissions are needed to change your profile picture')
			}
		}
	}

	const uploadImageAsync = (image) => {
		setIsLoading(true)
		axios
			.post('/courier/photo', { photo: image.base64 })
			.then(({ data: { user } }) => {
				setIsLoading(false)
				setUser(user)
				Alert.alert('Saved', 'Your profile picture has been updated!', [
					{
						text: 'OK',
						onPress: () => {
							setImageModalVisible(false)
							navigation.goBack()
						},
					},
				])
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response.data)
				Alert.alert('Error', 'Something went wrong while uploading your picture')
			})
	}

	const deleteImageAsync = () => {
		setIsLoading(true)
		axios
			.delete('/courier/photo')
			.then(({ data: { user } }) => {
				setIsLoading(false)
				setUser(user)
				Alert.alert('Deleted', 'Your profile picture has been removed!', [
					{
						text: 'OK',
						onPress: () => {
							setImageModalVisible(false)
							navigation.goBack()
						},
					},
				])
			})
			.catch(() => setIsLoading(false))
	}

	return (
		<>
			<Content>
				<ListItem itemDivider />
				<View style={{ padding: 16, flexDirection: 'row' }}>
					<TouchableOpacity onPress={() => setImageModalVisible(true)}>
						<CacheImage uri={user.photo} style={{ height: 100, width: 100, borderRadius: 50 }} />
						<MaterialCommunityIcons name='camera' size={26} style={{ position: 'absolute', bottom: 0, right: 0 }} />
					</TouchableOpacity>
				</View>
				<ListItem itemDivider />
				<Form style={{ backgroundColor: 'white' }}>
					<Item>
						<Label>Name:</Label>
						<Input value={name} onChangeText={(text) => setName(text)} />
					</Item>
					<Item>
						<Label>Phone:</Label>
						<Input value={phone} onChangeText={(text) => setPhone(text)} keyboardType={'phone-pad'} />
					</Item>
					<ListItem itemDivider />
					<ListItem itemDivider />
				</Form>
				<Button dark block disabled={isLoading} onPress={handleSavePress} style={{ margin: 16 }}>
					{isLoading ? <Spinner size='small' color='white' /> : <Text>Done</Text>}
				</Button>
			</Content>
			<Modal
				animationIn='fadeIn'
				animationOut='fadeOut'
				isVisible={imageModalVisible}
				onBackButtonPress={() => setImageModalVisible(false)}
				onBackdropPress={() => setImageModalVisible(false)}
				onRequestClose={() => setImageModalVisible(false)}
				swipeDirection={['up', 'down', 'left', 'right']}
				onSwipeComplete={() => setImageModalVisible(false)}
				style={{ margin: 0 }}
			>
				<>
					<View style={{ height: Layout.window.width, backgroundColor: 'white' }}>
						<CacheImage uri={user.photo} style={{ ...StyleSheet.absoluteFill }} />
					</View>
					<TouchableOpacity
						onPress={() => !isLoading && imageActionSheet.current.show()}
						style={{ padding: 16, backgroundColor: 'black', alignItems: 'center' }}
					>
						{isLoading ? <ActivityIndicator /> : <Text style={{ color: 'white' }}>Change</Text>}
					</TouchableOpacity>
				</>
			</Modal>

			<ActionSheet
				ref={imageActionSheet}
				options={['Choose photo', 'Remove photo', 'Cancel']}
				cancelButtonIndex={2}
				destructiveButtonIndex={1}
				onPress={(index) => {
					switch (index) {
						case 0:
							chooseImageAsync()
							break
						case 1:
							Alert.alert('Are you sure?', '', [
								{
									text: 'Cancel',
									style: 'cancel',
								},
								{
									text: 'Yes, remove it',
									onPress: () => deleteImageAsync(),
									style: 'destructive',
								},
							])
							break

						default:
							break
					}
				}}
			/>
		</>
	)
}
