import React, { useState, useRef, useEffect } from 'react'
import { View, Text, TextInput, StyleSheet, ActivityIndicator, Button } from 'react-native'
import Layout from '../../../constants/Layout'
import Modal from 'react-native-modal'
import { Feather, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import Colors from '../../../constants/Colors'
import axios from 'axios'
import AppContext from '../../../context/app-context'

export default ({ navigation, verifyCodeModalVisible, setVerifyCodeModalVisible, job }) => {
	const { setRefreshJobsList, refreshJobsList } = React.useContext(AppContext)
	const [isLoading, setIsLoading] = useState(false)
	const [success, setSuccess] = useState(false)
	const [fail, setFail] = useState(false)

	const [code, setCode] = useState('')

	const codeRef = useRef()

	useEffect(() => {
		if (code.split('').length == 5) {
			verifyCodeAsync()
		} else {
			setSuccess(false)
			setFail(false)
		}
	}, [code])

	// useEffect(() => {
	// 	if (verifyCodeModalVisible) {
	// 		codeRef.current.focus()
	// 	}
	// }, [verifyCodeModalVisible])

	const handleOnModalShow = () => {
		codeRef.current.focus()
	}

	const verifyCodeAsync = () => {
		setIsLoading(true)
		axios
			.post('/dispatch-jobs/verify-dropoff-code/' + job.id, { dropoff_verif_code: code })
			.then(({ data }) => {
				setIsLoading(false)
				if (data.status == 'SUCCESS') {
					setSuccess(true)
					setFail(false)
				} else if (data.status == 'FAIL') {
					setSuccess(false)
					setFail(true)
				}
			})
			.catch(({ response }) => {
				setIsLoading(false)
				console.log(response)
			})
	}

	const hideModal = () => {
		setVerifyCodeModalVisible(false)
		if (success) {
			setRefreshJobsList(!refreshJobsList)
			navigation.navigate('JobsStack', { screen: 'JobsTabs' })
		}
	}

	return (
		<Modal
			animationIn='slideInUp'
			isVisible={verifyCodeModalVisible}
			onBackButtonPress={() => hideModal()}
			onBackdropPress={() => hideModal()}
			onRequestClose={() => hideModal()}
			onModalShow={handleOnModalShow}
		>
			<View style={{ minHeight: 100, padding: 16, backgroundColor: 'white', borderRadius: 5 }}>
				<View style={{ alignItems: 'center', marginBottom: 16 }}>
					<Text style={{ fontSize: 15 }}>Verify Package Claim Code from Customer</Text>
				</View>
				<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
					<TextInput
						ref={codeRef}
						value={code}
						onChangeText={(number) => {
							setCode(number)
						}}
						keyboardType='numeric'
						maxLength={5}
						style={{ height: 0, width: 0 }}
					/>
					<View style={styles.numberBox}>
						<Text style={{ fontSize: 40 }}>{code.split('')[0] ? code.split('')[0] : ''}</Text>
					</View>
					<View style={styles.numberBox}>
						<Text style={{ fontSize: 40 }}>{code.split('')[1] ? code.split('')[1] : ''}</Text>
					</View>
					<View style={styles.numberBox}>
						<Text style={{ fontSize: 40 }}>{code.split('')[2] ? code.split('')[2] : ''}</Text>
					</View>
					<View style={styles.numberBox}>
						<Text style={{ fontSize: 40 }}>{code.split('')[3] ? code.split('')[3] : ''}</Text>
					</View>
					<View style={styles.numberBox}>
						<Text style={{ fontSize: 40 }}>{code.split('')[4] ? code.split('')[4] : ''}</Text>
					</View>
				</View>
				<View style={{ alignItems: 'center', marginTop: 16 }}>
					{isLoading && <ActivityIndicator />}
					{success && (
						<>
							<Text style={{ fontSize: 18, color: 'green' }}>Code is Valid! This job is complete</Text>
							<Button title='OK' onPress={hideModal} />
						</>
					)}
					{fail && (
						<>
							<Text style={{ fontSize: 18, color: 'red' }}>Code is Invalid!</Text>
							<Button title='OK' onPress={hideModal} />
						</>
					)}
				</View>
			</View>
		</Modal>
	)
}

const styles = StyleSheet.create({
	// numberInput: { height: 70, width: 60, borderWidth: 2, borderColor: Colors.black[500], textAlign: 'center', fontSize: 40 },
	numberBox: {
		flex: 1,
		marginHorizontal: 5,
		height: 70,
		borderWidth: 2,
		borderColor: Colors.black[500],
		justifyContent: 'center',
		alignItems: 'center',
	},
	// numberText: { height: 70, width: 60, borderWidth: 2, borderColor: Colors.black[500]}
})
