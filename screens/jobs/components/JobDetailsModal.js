import React, { useState, useContext, useEffect } from 'react'
import { View, TouchableOpacity, Alert, ActivityIndicator, SafeAreaView } from 'react-native'
import Layout from '../../../constants/Layout'
import Modal from 'react-native-modal'
import AppContext from '../../../context/app-context'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import Colors from '../../../constants/Colors'
import axios from 'axios'
import ActionSheet from 'react-native-actionsheet'
import GetDirections from 'react-native-google-maps-directions'
import moment from 'moment'
import VerifyCodeModal from './VerifyCodeModal'
import * as Location from 'expo-location'
import { Linking } from 'expo'
import * as Permissions from 'expo-permissions'
import * as TaskManager from 'expo-task-manager'
import { Content, List, ListItem, Left, Right, Text, Body } from 'native-base'

const LOCATION_TASK_NAME = 'background-location-task'

export default ({ navigation, detailsModalVisible, setDetailsModalVisible, job, setJob, beginNavigationToShop }) => {
	const {
		user,
		currentCoords,
		setPickupRouteParams,
		setdropOffRouteParams,
		pickupRouteParams,
		dropOffRouteParams,
		refreshJobsList,
		setRefreshJobsList,
	} = useContext(AppContext)
	const pickupTripActionSheet = React.useRef()
	const dropoffTripActionSheet = React.useRef()
	const completeJobActionSheet = React.useRef()
	const cancelJobActionSheet = React.useRef()
	const jobActionSheet = React.useRef()
	const [isLoading, setIsLoading] = useState(false)
	const [verifyCodeModalVisible, setVerifyCodeModalVisible] = useState(false)
	const [isCancellingJob, setIsCancellingJob] = useState(false)
	const [locationWatcher, setLocationWatcher] = useState(null)

	const navData = {
		params: [
			{
				key: 'travelmode',
				value: 'driving', // may be "walking", "bicycling" or "transit" as well
			},
			{
				key: 'dir_action',
				value: 'navigate', // this instantly initializes navigation using the given travel mode
			},
		],
	}

	// useEffect(() => {
	// 	console.log(pickupRouteParams.duration)
	// }, [])

	const cancelJobAsync = () => {
		setIsCancellingJob(true)
		axios
			.patch('/courier/cancel/' + job.id)
			.then(({ data }) => {
				Alert.alert(
					'Job Cancelled!',
					'This job has been removed from your list of upcoming jobs',
					[
						{
							text: 'Ok',
							onPress: () => {
								setDetailsModalVisible(false)
								navigation.navigate('AcceptedJobs')
							},
						},
					],
					{ cancelable: false }
				)
				setIsCancellingJob(false)
				setRefreshJobsList(!refreshJobsList)
			})
			.catch((err) => {
				setIsCancellingJob(false)
				console.log(err.response)
			})
	}

	const startPickupTrip = () => {
		// beginNavigationToShop()
		// return
		axios
			.get('/dispatch-jobs/start-pickup-trip/' + job.id)
			.then(({ data }) => {
				setJob(data)
				GetDirections({ destination: job.pickup_position, ...navData })
			})
			.catch((err) => console.log(err))
	}

	const startDropoffTrip = () => {
		// if (!job.pickup_code_verified_at) {
		// 	Alert.alert('Package not collected', 'Please confirm you have claimed the package before proceeding to the delivery address')
		// 	// loadJobsAsync()
		// 	return
		// }
		axios
			.get('/dispatch-jobs/start-dropoff-trip/' + job.id)
			.then(({ data }) => {
				if (data == 'NOT_CLAIMED') {
					Alert.alert(
						'Order not picked up!',
						'Please use the Package Claim Code to collect this order from the shop before proceeding to the delivery address'
					)
					return
				}
				setJob(data)
				GetDirections({ destination: job.dropoff_position, ...navData })
				// startLiveTrackingAsync()
			})
			.catch(({ response }) => console.log(response))
	}

	const startLiveTrackingAsync = async () => {
		// await Location.startLocationUpdatesAsync(LOCATION_TASK_NAME, {
		// 	accuracy: Location.Accuracy.High,
		// 	distanceInterval: 0,
		// 	timeInterval: 1000,
		// 	showsBackgroundLocationIndicator: true
		// })
		// let watcher = await Location.watchPositionAsync(
		// 	{
		// 		accuracy: Location.Accuracy.High,
		// 		distanceInterval: 0,
		// 		timeInterval: 1000
		// 	},
		// 	location => {
		// 		console.log(location)
		// 	}
		// )
		let watcher = setInterval(async () => {
			let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.High })
			console.log(location)
		}, 1000)
		setLocationWatcher(watcher)
		console.log('watching started')
	}

	const stopLiveTrackingAsync = async () => {
		// locationWatcher.remove()
		clearInterval(locationWatcher)
		console.log('watching stopped')
		// await Location.stopLocationUpdatesAsync(LOCATION_TASK_NAME)
	}

	const verifyPackageClaimCode = () => {
		setDetailsModalVisible(false)
		stopLiveTrackingAsync()
		setTimeout(() => {
			setVerifyCodeModalVisible(true)
		}, 500)
	}

	const jobActions = (index) => {
		let actions = [startPickupTrip, startDropoffTrip, verifyPackageClaimCode]
		return actions[index] && actions[index]()
	}

	const placeCallAsync = (number) => {
		Linking.openURL(`tel:${number}`)
	}

	return (
		<>
			<Modal
				animationIn='slideInUp'
				style={{ margin: 0, justifyContent: 'flex-end' }}
				isVisible={detailsModalVisible}
				onBackButtonPress={() => setDetailsModalVisible(false)}
				onBackdropPress={() => setDetailsModalVisible(false)}
				onRequestClose={() => setDetailsModalVisible(false)}
				swipeDirection='down'
				onSwipeComplete={() => setDetailsModalVisible(false)}
				propagateSwipe
			>
				<View style={{ height: Layout.window.height * 0.85, backgroundColor: 'white' }}>
					<View style={{ padding: 16, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
						<Text>Job (Order {job.order_ref} )</Text>
						<Text onPress={() => setDetailsModalVisible(false)}>
							<MaterialCommunityIcons name='close-circle' size={26} />
						</Text>
					</View>
					<SafeAreaView style={{ flex: 1 }}>
						<Content>
							<List style={{ backgroundColor: 'color' }}>
								<ListItem itemDivider>
									<Text>Pickup Info</Text>
								</ListItem>
								<ListItem>
									<Body>
										<Text note>ADDRESS</Text>
										<Text>{job.pickup_address}</Text>
									</Body>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='home-circle' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Shop name</Text>
									</Body>
									<Right>
										<Text>{job.pickup_name}</Text>
									</Right>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='alert-circle-outline' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Order Status</Text>
									</Body>
									<Right>
										<Text>{job.order_status}</Text>
									</Right>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='calendar-clock' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Pickup due</Text>
									</Body>
									<Right>
										<Text>{moment(`${job.pickup_due}`, 'YYYY-MM-DD hh:mm:ss').calendar()}</Text>
									</Right>
								</ListItem>
								<ListItem last icon onPress={() => placeCallAsync(job.pickup_phone)}>
									<Right>
										<MaterialCommunityIcons name='phone-outgoing' size={20} color='green' />
									</Right>
									<Body>
										<Text>Call Shop</Text>
									</Body>
									<Right>
										<Text>{job.pickup_phone}</Text>
									</Right>
								</ListItem>

								<ListItem itemDivider />
								<ListItem itemDivider>
									<Text>Drop-off Info</Text>
								</ListItem>
								<ListItem>
									<Body>
										<Text note>ADDRESS</Text>
										<Text>{job.dropoff_address}</Text>
									</Body>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='calendar-clock' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Drop-off due</Text>
									</Body>
									<Right>
										<Text>{moment(`${job.due_date} ${job.due_time[0]}`, 'YYYY-MM-DD hh:mm a').calendar()}</Text>
									</Right>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='account' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Customer name</Text>
									</Body>
									<Right>
										<Text>{job.dropoff_name}</Text>
									</Right>
								</ListItem>
								<ListItem icon onPress={() => placeCallAsync(job.dropoff_phone)}>
									<Right>
										<MaterialCommunityIcons name='phone-outgoing' size={20} color='green' />
									</Right>
									<Body>
										<Text>Call Customer</Text>
									</Body>
									<Right>
										<Text>{job.dropoff_phone}</Text>
									</Right>
								</ListItem>
								<ListItem last>
									<Body>
										<Text note>DROP-OFF NOTE</Text>
										<Text>{job.delivery_note}</Text>
									</Body>
								</ListItem>

								<ListItem itemDivider />
								<ListItem itemDivider>
									<Text>More Info</Text>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='progress-clock' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Est. duration</Text>
									</Body>
									<Right>
										<Text>~{Math.round(job.duration / 60)} mins</Text>
									</Right>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='map-marker-distance' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Est. distance</Text>
									</Body>
									<Right>
										<Text>~{(job.distance / 1000).toFixed(1)} km</Text>
									</Right>
								</ListItem>
								<ListItem icon>
									<Right>
										<MaterialCommunityIcons name='barcode' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Package Claim Code</Text>
									</Body>
									<Right>
										<Text>{job.pickup_verif_code}</Text>
									</Right>
								</ListItem>
								<ListItem icon last>
									<Right>
										<MaterialCommunityIcons name='cash-multiple' size={20} color='gray' />
									</Right>
									<Body>
										<Text>Exp. Earning</Text>
									</Body>
									<Right>
										<Text>
											{job.currency_symbol} {(job.earning / 100).toFixed(2)}
										</Text>
									</Right>
								</ListItem>
							</List>

							<TouchableOpacity
								onPress={() => cancelJobActionSheet.current.show()}
								style={{...Layout.styles.centerRow, borderRadius: 10, paddingVertical: 10, marginHorizontal: 25, marginTop: 40, marginBottom: 10 }}
							>
								{isCancellingJob ? (
									<ActivityIndicator color='black' />
								) : (
									<>
										<Ionicons name='ios-close-circle' size={20} color={Colors.red[600]} />
										<Text style={{ marginLeft: 5, fontWeight: 'bold', color: Colors.red[600] }}>Cancel Job</Text>
									</>
								)}
							</TouchableOpacity>

						</Content>
						{/* Accept / Decline Buttons */}
						<View style={{ flexDirection: 'row', padding: 16 }}>
							{/* <TouchableOpacity
								onPress={() => cancelJobActionSheet.current.show()}
								style={{ flex: 2, ...Layout.styles.centerRow, borderRadius: 5, backgroundColor: Colors.red[200], marginRight: 15 }}
							>
								{isCancellingJob ? (
									<ActivityIndicator color='black' />
								) : (
									<>
										<Ionicons name='ios-close-circle' size={20} color={Colors.red[600]} />
										<Text style={{ marginLeft: 5, fontWeight: 'bold', color: Colors.red[600] }}>Cancel Job</Text>
									</>
								)}
							</TouchableOpacity> */}
							<TouchableOpacity
								onPress={() => jobActionSheet.current.show()}
								style={{ flex: 3, ...Layout.styles.center, padding: 12, backgroundColor: Colors.green[300], borderRadius: 5 }}
							>
								<Text style={{ color: Colors.green[800], fontWeight: '600' }}>Actions</Text>
							</TouchableOpacity>
						</View>
					</SafeAreaView>
				</View>
			</Modal>

			<ActionSheet
				ref={pickupTripActionSheet}
				options={['Head to pickup location', 'Cancel']}
				cancelButtonIndex={1}
				onPress={(index) => (index == 0 ? startPickupTrip(job.pickup_position) : null)}
			/>

			<ActionSheet
				ref={dropoffTripActionSheet}
				options={['Head to dropoff location', 'Cancel']}
				cancelButtonIndex={1}
				onPress={(index) => (index == 0 ? startDropoffTrip(job.dropoff_position) : null)}
			/>

			<ActionSheet
				ref={completeJobActionSheet}
				options={['Verify package claim code', 'Cancel']}
				cancelButtonIndex={1}
				onPress={(index) => (index == 0 ? verifyPackageClaimCode() : null)}
			/>

			<ActionSheet
				ref={cancelJobActionSheet}
				options={['Cancel Job', 'Cancel']}
				cancelButtonIndex={1}
				destructiveButtonIndex={0}
				onPress={(index) => (index == 0 ? cancelJobAsync() : null)}
			/>

			<ActionSheet
				ref={jobActionSheet}
				options={['Head to Pick-Up Location', 'Head to Drop-Off Location', 'Verify Package Claim Code', 'Cancel']}
				cancelButtonIndex={3}
				// destructiveButtonIndex={3}
				onPress={(index) => jobActions(index)}
			/>

			<VerifyCodeModal
				navigation={navigation}
				verifyCodeModalVisible={verifyCodeModalVisible}
				setVerifyCodeModalVisible={setVerifyCodeModalVisible}
				job={job}
			/>
		</>
	)
}

// TaskManager.defineTask(LOCATION_TASK_NAME, async ({ data, error }) => {
// 	if (error) {
// 		console.log(error)
// 		return
// 	}
// 	if (data) {
// 		const { locations } = data
// 		let lat = locations[0].coords.latitude
// 		let long = locations[0].coords.longitude
// 		let user = JSON.parse(await AsyncStorage.getItem('USER')) || 'none'
// 		console.log(locations, user)

// 		// console.log("Received new locations for user = ", userId, locations);
// 	}
// })
