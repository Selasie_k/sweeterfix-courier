import React, { useState, useEffect, useContext } from 'react'
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import { useFocusEffect } from '@react-navigation/native'
import Layout from '../../constants/Layout'
import Colors from '../../constants/Colors'
// import { Content, List, ListItem } from 'native-base'
import axios from 'axios'
import AppContext from '../../context/app-context'
import { Feather, MaterialCommunityIcons } from '@expo/vector-icons'
import moment from 'moment'

export default ({ navigation }) => {
	const { activeJobs, setActiveJobs, user, currentCoords, accessingUserLocation } = useContext(AppContext)

	const [isLoading, setIsLoading] = useState(false)

	// useEffect(() => {
	// 	loadJobsAsync()
	// }, [])

	useFocusEffect(
		React.useCallback(() => {
			loadJobsAsync()
		}, [accessingUserLocation])
	)

	const loadJobsAsync = () => {
		if (!user.isAvailable || accessingUserLocation) return
		setIsLoading(true)
		const { latitude, longitude } = currentCoords
		axios
			.post('/dispatch-jobs', { position: { latitude, longitude } })
			.then(({ data }) => {
				setActiveJobs(data)
				setIsLoading(false)
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response.data)
			})
	}

	const renderUserOfflineView = () => (
		<View style={{ flex: 1, ...Layout.styles.center, padding: 20 }}>
			<MaterialCommunityIcons name="map-marker" size={90} color={Colors.black[400]} />
			<Text style={{ fontSize: 35, fontWeight: 'bold', textAlign: 'center' }}>You're offline</Text>
			<TouchableOpacity onPress={() => navigation.navigate('DashBoard')}>
				<Text style={{ marginVertical: 10, color: Colors.green[600], fontSize: 18 }}>Go online to see active jobs near you</Text>
			</TouchableOpacity>
		</View>
	)

	const renderEmptyActiveJobs = () => (
		<View style={{ flex: 1, ...Layout.styles.center, padding: 20 }}>
			<MaterialCommunityIcons name='truck-delivery' size={100} color={Colors.black[400]} />
			<Text style={{ fontSize: 20, textAlign: 'center' }}>No active jobs around you at the moment</Text>
			<TouchableOpacity onPress={loadJobsAsync}>
				<Text style={{ marginTop: 20, color: Colors.green[600], fontSize: 15 }}>REFRESH</Text>
			</TouchableOpacity>
		</View>
	)

	const renderJob = (job, index) => (
		<TouchableOpacity onPress={() => navigation.navigate('ActiveJob', { jobId: job.id })}>
			<View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row' }}>
				<View style={{ width: 28 }}>
					{/* <Text>{index + 1}.</Text> */}
					<Text>{job.courier_pickup_distance} km</Text>
				</View>
				<View style={{ flex: 1 }}>
					<View style={{ ...Layout.styles.centerBetween, paddingBottom: 10 }}>
						<View style={{ flexDirection: 'row', flex: 1 }}>
							<Text style={{ color: Colors.black[600], paddingLeft: 5 }}>
								Job {job.order_ref} ({job.pickup_name})
								{/* Job {job.order_ref} ({job.pickup_name} - {job.courier_pickup_distance} km) */}
							</Text>
						</View>
						<Text style={{ color: Colors.black[500] }}>{moment().to(utcToLocal(job.created_at))}</Text>
					</View>

					<View style={{ flexDirection: 'row', marginRight: 10 }}>
						<View style={Layout.styles.center}>
							<MaterialCommunityIcons name='map-marker' size={20} color={Colors.red[800]} />
							<View style={{ flex: 1, width: 1, backgroundColor: 'black' }} />
						</View>
						<View style={{ paddingBottom: 10 }}>
							<Text style={{ fontSize: 16, paddingLeft: 5 }}>{job.pickup_address}</Text>
						</View>
					</View>
					<View style={{ flexDirection: 'row', marginRight: 10, marginBottom: 10 }}>
						<View>
							<MaterialCommunityIcons name='map-marker-radius' size={20} color={Colors.green[700]} />
						</View>
						<View>
							<Text style={{ fontSize: 16, paddingLeft: 5 }}>{job.dropoff_address}</Text>
						</View>
					</View>

					<View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
						<MaterialCommunityIcons name='clock' size={20} color={Colors.black[500]} />
						<Text style={{ color: Colors.black[700], marginLeft: 6 }}>
							{/* Delivery due  */}
							Pickup due {/* {moment(`${job.due_date} ${job.due_time[0]}`, 'YYYY-MM-DD hh:mm a').fromNow()} ( */}
							{/* {moment().calendar(moment(`${job.due_date} ${job.due_time[1]}`, 'YYYY-MM-DD hh:mm a'))}) */}
							{/* {moment(`${job.due_date} ${job.due_time[0]}`, 'YYYY-MM-DD hh:mm a').calendar()} */}
							{moment(`${job.pickup_due}`, 'YYYY-MM-DD hh:mm:ss').calendar()}
						</Text>
					</View>

					<View style={{ ...Layout.styles.centerBetween }}>
						<View style={[Layout.styles.centerRow]}>
							<MaterialCommunityIcons name='square-inc-cash' size={16} color={Colors.green[500]} />
							<Text style={{ color: Colors.green[500], fontWeight: '600', paddingLeft: 5 }}>
								{job.currency_symbol}
								{(job.earning / 100).toFixed(2)}
							</Text>
						</View>
						<View style={Layout.styles.centerRow}>
							<MaterialCommunityIcons name='map-marker-distance' size={16} color={Colors.black[500]} />
							<Text style={{ color: Colors.black[500], paddingLeft: 5 }}>
								~{(job.distance / 1000 + job.courier_pickup_distance).toFixed(1)} km Transit
							</Text>
						</View>
						{/* <View style={Layout.styles.centerRow}>
							<MaterialCommunityIcons name='bus-clock' size={16} color={Colors.black[500]} />
							<Text style={{ color: Colors.black[500], paddingLeft: 5 }}>{Math.round(job.duration / 60)} mins job</Text>
						</View> */}
					</View>
				</View>
				{/* <View style={{ width: 20, ...Layout.styles.center }}>
					<Feather name='chevron-right' size={20} />
				</View> */}
			</View>
		</TouchableOpacity>
	)

	if (accessingUserLocation) return <ActivityIndicator style={{ flex: 1, ...Layout.styles.center }} color='black' />

	if (!user.isAvailable) return renderUserOfflineView()

	if (!isLoading && activeJobs.length < 1) return renderEmptyActiveJobs()

	return (
		<FlatList
			data={activeJobs}
			refreshing={isLoading}
			onRefresh={loadJobsAsync}
			renderItem={({ item, index }) => renderJob(item, index)}
			keyExtractor={(item) => item.id.toString()}
			ItemSeparatorComponent={() => <View style={{ height: 2, backgroundColor: '#e3e3e3' }} />}
		/>
	)
}

const utcToLocal = (date) => moment.utc(date).local()
