import React, { useState, useEffect, useContext } from 'react'
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator, StyleSheet, StatusBar } from 'react-native'
import Layout from '../../constants/Layout'
import Config from '../../config/app'
import Colors from '../../constants/Colors'
import MapView, { Marker } from 'react-native-maps'
import * as Location from 'expo-location'
import MapStyle from '../../constants/MapStyle'
import MapViewDirections from 'react-native-maps-directions'
import axios from 'axios'
import AppContext from '../../context/app-context'
import { useFocusEffect } from '@react-navigation/native'
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons'
import moment from 'moment'

import JobDetailsModal from './components/JobDetailsModal'

const LOCATION_TASK_NAME = 'background-location-task'

export default ({ navigation, route }) => {
	const {
		user,
		currentCoords,
		setCurrentCoords,
		setPickupRouteParams,
		setdropOffRouteParams,
		pickupRouteParams,
		dropOffRouteParams,
		refreshJobsList,
		setRefreshJobsList,
	} = useContext(AppContext)

	const mapRef = React.useRef()

	const [isLoading, setIsLoading] = useState(false)
	const [barStyle, setBarStyle] = useState('light-content')
	const [detailsModalVisible, setDetailsModalVisible] = useState(false)
	const [job, setJob] = useState(null)

	const [locationWatcher, setLocationWatcher] = useState(null)

	useEffect(() => {
		loadJobAsync()
	}, [])

	useFocusEffect(
		React.useCallback(() => {
			setBarStyle('light-content')
			return () => setBarStyle('dark-content')
		}, [])
	)

	const loadJobAsync = () => {
		setIsLoading(true)
		axios
			.get('/dispatch-jobs/' + route.params.jobId)
			.then(({ data }) => {
				setJob(data)
				setIsLoading(false)
				setTimeout(() => {
					animateMapToTargetMarkers()
				}, 1000)
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response)
			})
	}

	const animateMapToTargetMarkers = () => {
		mapRef.current.fitToSuppliedMarkers(['PickUpMarker', 'CourierMarker', 'DropOffMarker'], {
			edgePadding: { left: 50, right: 50, top: 50, bottom: 50 },
			animated: true,
		})
	}

	const packageCollectedFromShop = () => !!job.pickup_code_verified_at

	const beginNavigationToShop = async () => {
		setDetailsModalVisible(false)
		// mapRef.current.animateCamera(
		// 	{
		// 		center: currentCoords,
		// 		zoom: 18,
		// 		pitch: 20,
		// 	},
		// 	{ duration: 1000 }
		// )

		startLocationHeartbeatAsync()
	}

	const startLocationHeartbeatAsync = async () => {
		let watcher = setInterval(async () => {
			// let location = await Location.getCurrentPositionAsync({ accuracy: Location.Accuracy.High })
			// setCurrentCoords({ ...location.coords, latitudeDelta: 20, longitudeDelta: 20 })
			// console.log(location)
			console.log('loading...')
		}, 2000)

		// let watcher = await Location.watchPositionAsync(
		// 	{
		// 		accuracy: Location.Accuracy.High,
		// 		distanceInterval: 0,
		// 		timeInterval: 1000,
		// 	},
		// 	({ coords }) => {
		// 		console.log(coords)
		// 		mapRef.current.animateCamera({ center: coords, zoom: 18 }, { duration: 1000 })
		// 		// setCurrentCoords({ ...coords, latitudeDelta: 20, longitudeDelta: 20 })
		// 	}
		// )
		setLocationWatcher(watcher)
		// console.log(watcher)
		// console.log('start interval ' + locationWatcher)
		// console.log('watching started')
	}

	const stopLocationHeartbeatAsync = async () => {
		// await Location.stopLocationUpdatesAsync(LOCATION_TASK_NAME)
		locationWatcher.remove()
		// console.log('end interval ' + locationWatcher)
		// clearInterval(locationWatcher)
		console.log('watching stopped')
	}

	if (isLoading) {
		return <ActivityIndicator style={{ flex: 1, ...Layout.styles.center }} />
	}

	if (!isLoading && !job) {
		return (
			<View style={{ flex: 1, ...Layout.styles.center }}>
				<Text style={{ fontSize: 25 }}>This job is no longer available</Text>
			</View>
		)
	}

	return (
		<View style={{ flex: 1 }}>
			<StatusBar barStyle={barStyle} />
			{/* <View style={{ flex: 1 }}> */}
			<View style={{ flex: 1 }}>
				<TouchableOpacity
					onPress={() => navigation.navigate('AcceptedJobs')}
					style={{
						position: 'absolute',
						top: Layout.statusBarHeight + 20,
						left: 20,
						height: 40,
						width: 40,
						borderRadius: 20,
						backgroundColor: 'black',
						zIndex: 100,
						...Layout.styles.center,
					}}
				>
					<Ionicons name='md-arrow-round-back' size={26} color='white' />
				</TouchableOpacity>
				<MapView
					style={{ ...StyleSheet.absoluteFillObject }}
					customMapStyle={MapStyle}
					ref={mapRef}
					provider='google'
					initialRegion={currentCoords}
					showsUserLocation
					showsCompass
					showsPointsOfInterest={false}
					loadingEnabled
				>
					{/* <Marker identifier='PickUpMarker' title='Pick up' coordinate={job.pickup_position}>
						<CustomMarker name='cupcake' iconColor={Colors.red[600]} bgColor={Colors.red[300]} />
					</Marker> */}
					{/* <Marker identifier='DropOffMarker' title='Drop off' coordinate={job.dropoff_position}>
						<CustomMarker name='home-map-marker' iconColor={Colors.green[800]} bgColor={Colors.green[500]} />
					</Marker> */}
					<Marker identifier='CourierMarker' title='You' coordinate={currentCoords}>
						<View></View>
						{/* <MaterialCommunityIcons name='map-marker' size={30} color='yellow' /> */}
					</Marker>

					{packageCollectedFromShop() ? (
						<>
							<Marker identifier='DropOffMarker' title='Drop off' coordinate={job.dropoff_position}>
								<CustomMarker name='home-map-marker' iconColor={Colors.green[800]} bgColor={Colors.green[500]} />
							</Marker>
							<MapViewDirections
								origin={currentCoords}
								destination={job.dropoff_position}
								apikey={Config.google.MAPS_APIKEY}
								strokeWidth={6}
								strokeColor={Colors.red[500]}
								// onReady={(route) => setPickupRouteParams(route)}
							/>
						</>
					) : (
						<>
							<Marker identifier='DropOffMarker' title='Drop off' coordinate={job.dropoff_position}>
								<CustomMarker name='home-map-marker' iconColor={Colors.green[800]} bgColor={Colors.green[500]} />
							</Marker>
							<Marker identifier='PickUpMarker' title='Pick up' coordinate={job.pickup_position}>
								<CustomMarker name='cupcake' iconColor={Colors.red[600]} bgColor={Colors.red[300]} />
							</Marker>
							{/* pickup route */}
							<MapViewDirections
								origin={currentCoords}
								destination={job.pickup_position}
								apikey={Config.google.MAPS_APIKEY}
								strokeWidth={6}
								strokeColor={Colors.red[500]}
								onReady={(route) => setPickupRouteParams(route)}
							/>

							{/* drop off route */}
							<MapViewDirections
								origin={job.pickup_position}
								destination={job.dropoff_position}
								apikey={Config.google.MAPS_APIKEY}
								strokeWidth={6}
								strokeColor={Colors.green[700]}
								onReady={(route) => setdropOffRouteParams(route)}
							/>
						</>
					)}
				</MapView>
				<TouchableOpacity 
					onPress={() => setDetailsModalVisible(true)} 
					style={{ padding: 16, backgroundColor: 'green', 
						...Layout.styles.center, position: 'absolute', bottom: 0, left: 0, right: 0, borderRadius: 8, marginBottom: 20, marginHorizontal: 10 }}>
					<View>
						<Text style={{ color: 'white', fontWeight: '600' }}>View Job Details</Text>
					</View>
				</TouchableOpacity>
			</View>

			<JobDetailsModal
				navigation={navigation}
				job={job}
				setJob={setJob}
				detailsModalVisible={detailsModalVisible}
				setDetailsModalVisible={setDetailsModalVisible}
				beginNavigationToShop={beginNavigationToShop}
			/>
		</View>
	)
}

const CustomMarker = ({ name, iconColor, bgColor }) => (
	<View style={Layout.styles.center}>
		<View style={{ ...Layout.styles.center, height: 30, width: 30, borderRadius: 15, backgroundColor: bgColor }}>
			<MaterialCommunityIcons name={name} size={20} color={iconColor} />
		</View>
		<View style={{ width: 2, height: 25, backgroundColor: bgColor }} />
	</View>
)
