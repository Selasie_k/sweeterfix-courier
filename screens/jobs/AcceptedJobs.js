import React, { useState, useEffect, useContext } from 'react'
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator } from 'react-native'
import Layout from '../../constants/Layout'
import Colors from '../../constants/Colors'
import { useFocusEffect } from '@react-navigation/native'
// import { Content, List, ListItem } from 'native-base'
import axios from 'axios'
import AppContext from '../../context/app-context'
import { Feather, MaterialCommunityIcons } from '@expo/vector-icons'
import moment from 'moment'

export default ({ navigation }) => {
	const { activeJobs, setActiveJobs, user, refreshJobsList, upcomingJobs, setUpcomingJobs } = useContext(AppContext)
	const [isLoading, setIsLoading] = useState(false)

	useEffect(() => {
		loadJobsAsync()
	}, [refreshJobsList])

	// useFocusEffect(
	// 	React.useCallback(() => {
	// 		loadJobsAsync()
	// 	}, [])
	// )

	const loadJobsAsync = () => {
		setIsLoading(true)
		axios
			.get('/courier/upcoming-jobs')
			.then(({ data }) => {
				setUpcomingJobs(data)
				setIsLoading(false)
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response)
			})
	}

	const renderEmptyUpcomingJobs = () => (
		<View style={{ flex: 1, ...Layout.styles.center, padding: 20 }}>
			<MaterialCommunityIcons name='calendar-clock' size={100} color={Colors.black[400]} />
			<Text style={{ fontSize: 20, textAlign: 'center' }}>No upcoming jobs</Text>
			<TouchableOpacity onPress={loadJobsAsync}>
				<Text style={{ marginTop: 20, color: Colors.green[600], fontSize: 15 }}>REFRESH</Text>
			</TouchableOpacity>
		</View>
	)

	const renderJob = (job, index) => (
		<TouchableOpacity onPress={() => navigation.navigate('AcceptedJob', { jobId: job.id })}>
			<View style={{ padding: 16, backgroundColor: 'white', flexDirection: 'row' }}>
				<View style={{ width: 20 }}>
					<Text>{index + 1}.</Text>
				</View>
				<View style={{ flex: 1 }}>
					<View style={{ ...Layout.styles.centerBetween, paddingBottom: 10 }}>
						<View style={{ flexDirection: 'row' }}>
							<Text style={{ color: Colors.black[600], paddingLeft: 5 }}>{job.pickup_name}</Text>
						</View>
						<Text style={{ color: Colors.black[500] }}>{moment().to(utcToLocal(job.created_at))}</Text>
					</View>

					<View
						style={{
							flexDirection: 'row',
							justifyContent: 'space-between',
							marginBottom: 10,
							padding: 10,
							borderRadius: 5,
							backgroundColor: job.order_status == 'Ready' ? Colors.green[300] : Colors.black[100],
						}}
					>
						<Text style={{ color: Colors.black[800] }}>
							Order Status: <Text style={{ fontWeight: 'bold' }}>{job.order_status}</Text>{' '}
						</Text>
						<Text style={{ color: Colors.black[800], paddingLeft: 5 }}>
							Ref: <Text style={{ fontWeight: 'bold' }}>{job.order_ref}</Text>
						</Text>
					</View>
					{/* <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
						<MaterialCommunityIcons name='alert-box' size={20} color={Colors.black[500]} />
						<Text style={{ color: Colors.black[700], marginLeft: 6 }}>
							Order Status: <Text style={{ fontWeight: 'bold' }}>{job.order_status}</Text>{' '}
						</Text>
					</View> */}
					<View style={{ flexDirection: 'row', marginRight: 10 }}>
						<View style={Layout.styles.center}>
							<MaterialCommunityIcons name='map-marker' size={20} color={Colors.red[800]} />
							<View style={{ flex: 1, width: 1, backgroundColor: 'black' }} />
						</View>
						<View style={{ paddingBottom: 10 }}>
							<Text style={{ fontSize: 16, paddingLeft: 5 }}>{job.pickup_address}</Text>
						</View>
					</View>
					<View style={{ flexDirection: 'row', marginRight: 10, marginBottom: 10 }}>
						<View>
							<MaterialCommunityIcons name='map-marker-radius' size={20} color={Colors.green[700]} />
						</View>
						<View>
							<Text style={{ fontSize: 16, paddingLeft: 5 }}>{job.dropoff_address}</Text>
						</View>
					</View>

					<View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 10 }}>
						<MaterialCommunityIcons name='calendar-clock' size={20} color={Colors.black[500]} />
						<Text style={{ color: Colors.black[700], marginLeft: 6 }}>
							{/* Delivery due  */}
							Delivery due {moment(`${job.due_date} ${job.due_time[0]}`, 'YYYY-MM-DD hh:mm a').calendar()}
						</Text>
					</View>

					<View style={{ ...Layout.styles.centerBetween }}>
						<View style={[Layout.styles.centerRow]}>
							<MaterialCommunityIcons name='square-inc-cash' size={16} color={Colors.green[500]} />
							<Text style={{ color: Colors.green[500], fontWeight: '600', paddingLeft: 5 }}>
								{job.currency_symbol}
								{(job.earning / 100).toFixed(2)}
							</Text>
						</View>
						<View style={Layout.styles.centerRow}>
							<MaterialCommunityIcons name='map-marker-distance' size={16} color={Colors.black[500]} />
							<Text style={{ color: Colors.black[500], paddingLeft: 5 }}>{(job.distance / 1000).toFixed(1)} km transit</Text>
						</View>
						<View style={Layout.styles.centerRow}>
							<MaterialCommunityIcons name='bus-clock' size={16} color={Colors.black[500]} />
							<Text style={{ color: Colors.black[500], paddingLeft: 5 }}>{Math.round(job.duration / 60)} mins job</Text>
						</View>
					</View>
				</View>
				{/* <View style={{ width: 20, ...Layout.styles.center }}>
					<Feather name='chevron-right' size={20} />
				</View> */}
			</View>
		</TouchableOpacity>
	)

	if (!isLoading && upcomingJobs.length < 1) return renderEmptyUpcomingJobs()

	return (
		<FlatList
			data={upcomingJobs}
			refreshing={isLoading}
			onRefresh={loadJobsAsync}
			renderItem={({ item, index }) => item.courier_id == user.id && renderJob(item, index)}
			keyExtractor={(item) => item.id.toString()}
			ItemSeparatorComponent={() => <View style={{ height: 1, backgroundColor: '#e3e3e3' }} />}
		/>
	)
}

const utcToLocal = (date) => moment.utc(date).local()
