import React, { useState, useEffect, useContext } from 'react'
import { View, Text, FlatList, TouchableOpacity, ActivityIndicator, StyleSheet, Alert } from 'react-native'
import Layout from '../../constants/Layout'
import Config from '../../config/app'
import Colors from '../../constants/Colors'
import MapView, { Marker } from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import MapStyle from '../../constants/MapStyle'
import axios from 'axios'
import AppContext from '../../context/app-context'
import { MaterialCommunityIcons, Ionicons } from '@expo/vector-icons'
import moment from 'moment'
import ActionSheet from 'react-native-actionsheet'

export default ({ navigation, route }) => {
	const {
		activeJobs,
		setActiveJobs,
		currentCoords,
		setPickupRouteParams,
		setdropOffRouteParams,
		pickupRouteParams,
		dropOffRouteParams,
		refreshJobsList,
		setRefreshJobsList,
	} = useContext(AppContext)

	const mapRef = React.useRef()
	const actionSheet = React.useRef()

	const [isLoading, setIsLoading] = useState(false)
	const [isAcceptingJob, setIsAcceptingJob] = useState(false)
	const [job, setJob] = useState(null)

	useEffect(() => {
		loadJobsAsync()
	}, [])

	const loadJobsAsync = () => {
		setIsLoading(true)
		axios
			.get('/dispatch-jobs/' + route.params.jobId)
			.then(({ data }) => {
				setJob(data)
				setIsLoading(false)
				setTimeout(() => {
					animateMapToTargetMarkers()
				}, 1000)
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response)
			})
	}

	const acceptJobAsync = () => {
		setIsAcceptingJob(true)
		axios
			.patch('/courier/accept/' + job.id)
			.then(({ data }) => {
				if (data.status == 'OK') {
					Alert.alert(
						'Success',
						'This job has been added to your list of upcoming jobs',
						[
							{ text: 'Back', onPress: () => navigation.goBack() },
							{ text: 'View job', onPress: () => navigation.navigate('AcceptedJob', { jobId: data.job_id }) },
						],
						{ cancelable: false }
					)
				}
				if (data.status == 'NOT_AVAILABLE') {
					Alert.alert(
						'Accepting Job Failed',
						'Sorry, this job has already been accepted by a different courier. Please try other available jobs',
						[{ text: 'Back', onPress: () => navigation.goBack() }],
						{ cancelable: false }
					)
				}
				setIsAcceptingJob(false)
				setRefreshJobsList(!refreshJobsList)
			})
			.catch((err) => {
				setIsAcceptingJob(false)
				console.log(err.response)
			})
	}

	const animateMapToTargetMarkers = () => {
		mapRef.current.fitToSuppliedMarkers(['PickUpMarker', 'CourierMarker', 'DropOffMarker'], {
			edgePadding: { left: 50, right: 50, top: 50, bottom: 50 },
			animated: true,
		})
	}

	if (isLoading) {
		return <ActivityIndicator style={{ flex: 1, ...Layout.styles.center }} />
	}

	if (!isLoading && !job) {
		return (
			<View style={{ flex: 1, ...Layout.styles.center }}>
				<Text style={{ fontSize: 25 }}>This job is no longer available</Text>
			</View>
		)
	}

	return (
		<View style={{ flex: 1 }}>
			{/* <View style={{ flex: 1 }}> */}
			<View style={{ flex: 1 }}>
				<MapView
					style={{ ...StyleSheet.absoluteFillObject }}
					customMapStyle={MapStyle}
					ref={mapRef}
					provider='google'
					initialRegion={currentCoords}
					showsUserLocation
					showsCompass
					showsPointsOfInterest={false}
					loadingEnabled
				>
					<Marker identifier='PickUpMarker' title='Pick up' coordinate={job.pickup_position}>
						<CustomMarker name='cupcake' iconColor={Colors.red[600]} bgColor={Colors.red[300]} />
					</Marker>
					<Marker identifier='DropOffMarker' title='Drop off' coordinate={job.dropoff_position}>
						<CustomMarker name='home-map-marker' iconColor={Colors.green[800]} bgColor={Colors.green[500]} />
					</Marker>
					<Marker identifier='CourierMarker' title='You' coordinate={currentCoords}>
						<View></View>
					</Marker>

					{/* pickup route */}
					<MapViewDirections
						origin={currentCoords}
						destination={job.pickup_position}
						apikey={Config.google.MAPS_APIKEY}
						strokeWidth={4}
						strokeColor={Colors.red[500]}
						onReady={({ distance, duration }) => setPickupRouteParams({ distance, duration })}
						// onReady={(res) => console.log(res)}
						onError={(error) => console.log(error)}
					/>

					{/* drop off route */}
					<MapViewDirections
						origin={job.pickup_position}
						destination={job.dropoff_position}
						apikey={Config.google.MAPS_APIKEY}
						strokeWidth={4}
						strokeColor={Colors.green[700]}
						onReady={({ distance, duration }) => setdropOffRouteParams({ distance, duration })}
						// onReady={(res) => console.log(res)}
						onError={(error) => console.log(error)}
					/>
				</MapView>
			</View>
			<View
				style={{
					// flex: 1,
					...Layout.styles.elevationShadow(5),
					backgroundColor: 'white',
					padding: 12,
				}}
			>
				{/* Upper Section */}
				<View style={{ flexDirection: 'row', paddingBottom: 12 }}>
					{/* Top Left */}
					<View style={{ flex: 1 }}>
						<Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>PICK UP</Text>
						<Text style={{ fontSize: 15 }}>{job.pickup_address}</Text>
					</View>
					{/* Top Right */}
					<View style={{ flex: 1 }}>
						<Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>DROP OFF</Text>
						<Text style={{ fontSize: 15 }}>{job.dropoff_address}</Text>
					</View>
				</View>

				{/* Middle Section */}
				<View style={{ flexDirection: 'row', paddingBottom: 12 }}>
					{/* Mid Left */}
					<View style={{ flex: 1 }}>
						{/* <Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>EST. JOB DURATION</Text>
						<Text style={{ fontSize: 15 }}>~{Math.round(job.duration / 60 + (pickupRouteParams?.duration || 0))} mins</Text> */}
						<Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>PICKUP DUE</Text>
						<Text style={{ fontSize: 15 }}>{moment(`${job.pickup_due}`, 'YYYY-MM-DD hh:mm:ss').calendar()} </Text>
					</View>
					{/* Mid Right */}
					<View style={{ flex: 1 }}>
						<Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>DROP-OFF DUE</Text>
						<Text style={{ fontSize: 15 }}>{moment(`${job.due_date} ${job.due_time[0]}`, 'YYYY-MM-DD hh:mm a').calendar()} </Text>
					</View>
				</View>

				{/* Lower Section */}
				<View style={{ flexDirection: 'row', paddingBottom: 12 }}>
					{/* Bottom Left */}
					<View style={{ flex: 1 }}>
						<Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>EST. TIME / DISTANCE</Text>
						{/* <Text style={{ fontSize: 15 }}>~{(job.distance / 1000 + pickupRouteParams?.distance).toFixed(1)} km</Text> */}
						<Text style={{ fontSize: 15 }}>
							{(job.distance / 1000 + (pickupRouteParams?.distance || 0)).toFixed(1)} km |{' '}
							{Math.round(job.duration / 60 + (pickupRouteParams?.duration || 0))} mins
						</Text>
						{/* <Text style={Layout.styles.text[100]}>
						{parseFloat(pickupRouteParams?.distance + dropOffRouteParams?.distance).toFixed(1)} km
					</Text> */}
					</View>

					{/* Bottom Right */}
					<View style={{ flex: 1 }}>
						<Text style={{ fontSize: 11, fontWeight: '600', color: Colors.black[500] }}>EARNING</Text>
						<Text style={{ fontSize: 15 }}>
							{job.currency_symbol} {job.earning / 100}
						</Text>
					</View>
				</View>

				{/* Accept / Decline Buttons */}
				<View style={{ flexDirection: 'row' }}>
					<TouchableOpacity
						onPress={() => navigation.navigate('JobsStack', { screen: 'JobsTabs' })}
						style={{ flex: 1, ...Layout.styles.centerRow, borderRadius: 5, backgroundColor: Colors.red[200], marginRight: 15 }}
					>
						<Ionicons name='ios-arrow-round-back' size={30} color={Colors.red[600]} />
						<Text style={{ marginLeft: 10, fontWeight: 'bold', color: Colors.red[600] }}>Go back</Text>
					</TouchableOpacity>

					<TouchableOpacity
						onPress={() => !isAcceptingJob && actionSheet.current.show()}
						style={{ flex: 2, ...Layout.styles.center, padding: 12, backgroundColor: Colors.green[300], borderRadius: 5 }}
					>
						{isAcceptingJob ? (
							<ActivityIndicator color='black' />
						) : (
							<Text style={{ color: Colors.green[800], fontWeight: '600' }}>Accept Job</Text>
						)}
					</TouchableOpacity>
				</View>
			</View>

			<ActionSheet
				ref={actionSheet}
				options={['Accept Job', 'Cancel']}
				cancelButtonIndex={1}
				// destructiveButtonIndex={user.isAvailable ? 0 : null}
				onPress={(index) => (index == 0 ? acceptJobAsync() : null)}
			/>
		</View>
	)
}

const CustomMarker = ({ name, iconColor, bgColor }) => (
	<View style={Layout.styles.center}>
		<View style={{ ...Layout.styles.center, height: 30, width: 30, borderRadius: 15, backgroundColor: bgColor }}>
			<MaterialCommunityIcons name={name} size={20} color={iconColor} />
		</View>
		<View style={{ width: 2, height: 25, backgroundColor: bgColor }} />
	</View>
)
