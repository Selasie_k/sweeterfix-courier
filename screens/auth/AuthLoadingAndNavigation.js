import React from 'react'
import { View, Text, SafeAreaView, AsyncStorage, ActivityIndicator, Platform, Alert } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs'
import { SplashScreen } from 'expo'
import AppContext from '../../context/app-context'
import axios from 'axios'
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import InitializeNotifications from '../../Notifications'
import app from '../../config/app'

import LoginScreen from './Login'
import RegisterScreen from './Register'
import Dashboard from '../dashboard/Dashboard'

import SettingsScreen from '../settings/Settings'
import EditProfileScreen from '../settings/EditProfile'
import ChangePasswordScreen from '../settings/ChangePassword'
import StripeWebViewScreen from '../settings/StripeWebView'
import SupportScreen from '../settings/Support'

import EarningsScreen from '../earnings/Earnings'
import EarningScreen from '../earnings/Earning'
import ActiveJobsScreen from '../jobs/ActiveJobs'
import ActiveJobScreen from '../jobs/ActiveJob'
import AcceptedJobsScreen from '../jobs/AcceptedJobs'
import AcceptedJobScreen from '../jobs/AcceptedJob'

const Stack = createStackNavigator()
const BottomTab = createBottomTabNavigator()
const TopTab = createMaterialTopTabNavigator()

export default ({ navigation }) => {
	const { user, setUser } = React.useContext(AppContext)
	const [isLoading, setIsLoading] = React.useState(false)

	React.useEffect(() => {
		setIsLoading(true)
		retrieveAuthTokenAsync()
	}, [])

	const retrieveAuthTokenAsync = () => {
		AsyncStorage.getItem('USER').then((data) => {
			if (data == null) {
				setIsLoading(false)
				return
			}
			validateApiTokenAsync(JSON.parse(data))
		})
	}

	const validateApiTokenAsync = ({ email, api_token }) => {
		axios
			.post('/courier/validate-token', { email, token: api_token })
			.then(({ data }) => {
				const { user } = data
				setUser(user)
				axios.defaults.headers.common['Authorization'] = `Bearer ${user.api_token}`

				setIsLoading(false)

				// if(Platform.OS == 'android'){
				// 	Alert.alert(`${isLoading} v${Platform.Version} press OK to close`)
				// }
			})
			.catch((err) => {
				console.log(err)
				setIsLoading(false)
			})
	}

	if (isLoading) {
		return (
			<SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 20 }}>
				<ActivityIndicator color='black' size={'large'} />
				<Text style={{ fontSize: 30, fontWeight: 'bold', textAlign: 'center', marginTop: 20 }}>SweeterFix Courier</Text>
				<Text style={{ textAlign: 'center', marginTop: 20 }}>Version {app.version}</Text>
			</SafeAreaView>
		)
	}

	return (
		<>
			{!user ? (
				<Stack.Navigator headerMode='none' mode='modal'>
					<Stack.Screen name='Login' component={LoginScreen} />
					<Stack.Screen name='Register' component={RegisterScreen} />
				</Stack.Navigator>
			) : (
				<>
					<HomeStack />
					<InitializeNotifications />
				</>
			)}
		</>
	)
}

const HomeStack = ({ navigation }) => {
	return (
		<BottomTab.Navigator
			initialRouteName={'JobsStack'}
			tabBarOptions={{
				activeTintColor: 'black',
				inactiveTintColor: 'gray',
			}}
		>
			<BottomTab.Screen
				name='DashboardStack'
				component={DashboardStack}
				options={{
					title: 'Dashboard',
					tabBarIcon: ({ focused, color, size }) => {
						let iconName = focused ? 'ios-speedometer' : 'md-speedometer'
						return <Ionicons name={iconName} size={size} color={color} />
					},
				}}
			/>
			<BottomTab.Screen
				name='JobsStack'
				component={JobsStack}
				options={{
					title: 'Jobs',
					tabBarIcon: ({ focused, color, size }) => {
						let iconName = focused ? 'truck-fast' : 'truck-delivery'
						return <MaterialCommunityIcons name={iconName} size={size} color={color} />
					},
				}}
			/>
			<BottomTab.Screen
				name='EariningsStack'
				component={EariningsStack}
				options={{
					title: 'Earinings',
					tabBarIcon: ({ focused, color, size }) => {
						let iconName = focused ? 'ios-wallet' : 'md-wallet'
						return <Ionicons name={iconName} size={size} color={color} />
					},
				}}
			/>
			<BottomTab.Screen
				name='SettingsStack'
				component={SettingsStack}
				options={{
					title: 'Settings',
					tabBarIcon: ({ focused, color, size }) => {
						let iconName = focused ? 'md-cog' : 'ios-cog'
						return <Ionicons name={iconName} size={size} color={color} />
					},
				}}
			/>
		</BottomTab.Navigator>
	)
}

const DashboardStack = ({ navigation }) => {
	return (
		<Stack.Navigator headerMode='none'>
			<Stack.Screen name='DashBoard' component={Dashboard} />
		</Stack.Navigator>
	)
}

const EariningsStack = ({ navigation }) => {
	return (
		<Stack.Navigator>
			<Stack.Screen name='Earnings' component={EarningsScreen} />
			<Stack.Screen name='Earning' component={EarningScreen} />
		</Stack.Navigator>
	)
}

const SettingsStack = ({ navigation }) => {
	return (
		<Stack.Navigator>
			<Stack.Screen name='Settings' component={SettingsScreen} />
			<Stack.Screen name='EditProfile' component={EditProfileScreen} options={{ title: 'Edit Profile' }} />
			<Stack.Screen name='ChangePassword' component={ChangePasswordScreen} options={{ title: 'Change Password' }} />
			<Stack.Screen name='StripeWebView' component={StripeWebViewScreen} options={{ title: 'Stripe Account' }} />
			<Stack.Screen name='SupportScreen' component={SupportScreen} options={{ title: 'Support' }} />
		</Stack.Navigator>
	)
}

const JobsStack = ({ navigation }) => {
	return (
		<Stack.Navigator>
			<TopTab.Screen name='JobsTabs' component={JobsTopTab} options={{ title: 'Jobs', headerStyle: { borderBottomWidth: 0 } }} />
			<TopTab.Screen name='ActiveJob' component={ActiveJobScreen} options={{ title: 'Job Details', headerShown: false }} />
			<TopTab.Screen name='AcceptedJob' component={AcceptedJobScreen} options={{ title: 'Job Details', headerShown: false }} />
		</Stack.Navigator>
	)
}

const JobsTopTab = ({ navigation }) => {
	const { activeJobs, upcomingJobs } = React.useContext(AppContext)
	return (
		<TopTab.Navigator>
			<TopTab.Screen
				name='Jobs'
				component={ActiveJobsScreen}
				options={{
					title: 'Active jobs near you',
					tabBarLabel: ({ focused, color }) => (
						<Text style={{ color }}>
							<Text style={{ fontWeight: 'bold' }}>{activeJobs.length}</Text> JOBS NEAR YOU
						</Text>
					),
				}}
			/>
			<TopTab.Screen
				name='AcceptedJobs'
				component={AcceptedJobsScreen}
				options={{
					title: 'My Jobs',
					tabBarLabel: ({ focused, color }) => (
						<Text style={{ color }}>
							<Text style={{ fontWeight: 'bold' }}>{upcomingJobs.length}</Text> UPCOMING JOBS
						</Text>
					),
				}}
			/>
		</TopTab.Navigator>
	)
}
