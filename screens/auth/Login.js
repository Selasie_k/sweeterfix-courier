import React, { useState, useContext } from 'react'
import {
	Text,
	View,
	Button,
	TextInput,
	TouchableWithoutFeedback,
	Alert,
	AsyncStorage,
	KeyboardAvoidingView,
	ActivityIndicator,
	StatusBar,
	ImageBackground,
	Image,
	Keyboard,
	StyleSheet,
	SafeAreaView,
	TouchableOpacity,
} from 'react-native'
import AppContext from '../../context/app-context'
import axios from 'axios'
import Config from '../../config/app'
import Layout from '../../constants/Layout'
import Colors from '../../constants/Colors'
import * as WebBrowser from 'expo-web-browser'

export default ({ navigation }) => {
	const {setUser} = useContext(AppContext)

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [errors, setErrors] = useState(null)
	const [isLoading, setIsLoading] = useState(false)

	const handleLoginPress = () => {
		if (isLoading) return

		if (email == '' || password == '') {
			Alert.alert('Please provide your email and password')
			return
		}

		setIsLoading(true)
		setErrors(null)

		axios
			.post('/courier/api_login', { email, password })
			.then(({ data }) => {
				const { user } = data
				storeUser(user)
			})
			.catch((err) => {
				setIsLoading(false)
				Alert.alert("We couldn't authenticate you", ' Please check your credentials and try again')
				if (err.response?.data.errors) {
					setErrors(err.response.data.errors)
					return
				}
			})
	}

	const storeUser = (user) => {
		AsyncStorage.setItem('USER', JSON.stringify(user)).then(() => {
			axios.defaults.headers.common['Authorization'] = `Bearer ${user.api_token}`
			setUser(user)
			// context.setIsSignedIn(true)
		})
	}

	const handleForgotPasswordPress = () => {
		WebBrowser.openBrowserAsync(`${Config.http.baseUrl2}/password/reset?broker=couriers`)
	}

	return (
		<SafeAreaView style={{ flex: 1 }}>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
				<View
					style={{
						flex: 1,
						justifyContent: 'center',
						alignItems: 'center',
					}}
				>
					<StatusBar barStyle={Layout.isIOS ? 'dark-content' : 'light-content'} />

					<KeyboardAvoidingView behavior='position' style={{}}>
						<View
							style={{
								padding: 16,
								marginBottom: 10,
								width: Layout.window.width - 40,
								backgroundColor: 'white',
								borderRadius: 10,
								...Layout.styles.elevationShadow(3),
							}}
						>
							<Image
								source={require('../../assets/images/app-icon-transparent.png')}
								style={{
									width: 100,
									height: 100,
									alignSelf: 'center',
									marginTop: -66,
								}}
							/>
							<Text style={{ fontSize: 30, fontWeight: 'bold', paddingBottom: 10 }}>Login</Text>

							<TextInput
								autoCapitalize={'none'}
								style={styles.inputBox}
								placeholder='Email'
								onChangeText={(text) => setEmail(text)}
								value={email}
								keyboardType='email-address'
								textContentType='username'
							/>
							<Text style={styles.error}>{errors && errors.email && errors.email[0]}</Text>

							<TextInput
								autoCapitalize={'none'}
								style={styles.inputBox}
								placeholder='Password'
								onChangeText={(text) => setPassword(text)}
								value={password}
								textContentType='password'
								secureTextEntry
							/>
							<Text style={styles.error}>{errors && errors.password && errors.password[0]}</Text>

							<TouchableOpacity
								style={{
									padding: 16,
									marginTop: 10,
									justifyContent: 'center',
									alignItems: 'center',
									backgroundColor: Colors.orange,
									borderRadius: 10,
								}}
								onPress={handleLoginPress}
							>
								{isLoading ? (
									<ActivityIndicator color='white' />
								) : (
									<Text
										style={{
											fontWeight: 'bold',
											fontSize: 20,
											color: 'white',
										}}
									>
										Login
									</Text>
								)}
							</TouchableOpacity>

							<Text style={{ marginTop: 20, padding: 10, alignSelf: 'center' }} onPress={() => navigation.navigate('Register')}>
								New SweeterFix driver? <Text style={{ fontWeight: 'bold' }}>Sign Up</Text>
							</Text>
						</View>
					</KeyboardAvoidingView>
				</View>
			</TouchableWithoutFeedback>
			<Text style={{ textAlign: 'center', marginBottom: 10, fontSize: 15, padding: 10 }} onPress={handleForgotPasswordPress}>
				Forgot Password?
			</Text>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	inputBox: {
		backgroundColor: '#eee',
		height: 50,
		padding: 10,
		marginVertical: 10,
		borderRadius: 5,
	},
	error: {
		marginTop: -10,
		color: 'red',
	},
})
