import React, { useState, useContext, useEffect } from 'react'
import {
	Text,
	View,
	TextInput,
	TouchableWithoutFeedback,
	Alert,
	TouchableOpacity,
	KeyboardAvoidingView,
	ActivityIndicator,
	StatusBar,
	Image,
	Keyboard,
	StyleSheet,
} from 'react-native'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Layout from '../../constants/Layout'
import AppContext from '../../context/app-context'
import axios from 'axios'
import Colors from '../../constants/Colors'
import Config from '../../config/app'
import CountriesList from '../../config/CountriesList'
import Modal from 'react-native-modal'
// import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import { Content, List, ListItem, Right, Left, Body } from 'native-base'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import extractAddressComponents from '../../helpers/AddressComponents'
import { MaterialCommunityIcons, Feather as FeatherIcons } from '@expo/vector-icons'

export default ({ navigation }) => {
	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [phone, setPhone] = useState('')
	const [address, setAddress] = useState(null)
	const [errors, setErrors] = useState(null)
	const [isLoading, setIsLoading] = useState(false)
	const [selectedCountry, setSelectedCountry] = useState(null)
	const [countriesModalVisible, setCountriesModalVisible] = useState(false)

	const [location, setLocation] = useState(null)
	const [errorMsg, setErrorMsg] = useState(null)

	const [addressModalVisible, setAddressModalVisible] = useState(false)
	const [applicationCompleteModalVisible, setApplicationCompleteModalVisible] = useState(false)

	const context = useContext(AppContext)
	let addressInputRef = React.useRef()

	useEffect(() => {
		;(async () => {
			const { status } = await Location.requestPermissionsAsync()
			if (status !== 'granted') {
				Alert.alert(
					'Location permissions not granted',
					'Please enable location permissions for this app to continue',
					[{ text: 'Ok', onPress: () => navigation.navigate('Login') }],
					{ cancelable: false }
				)
				return
			}
			getCurrentLocationAsync()
		})()
	}, [])

	const getCurrentLocationAsync = async () => {
		const location = await Location.getLastKnownPositionAsync()
		const [{ country, region, city, street, name, postalCode, isoCountryCode }] = await Location.reverseGeocodeAsync(location.coords)
		setAddress({ country, city, state: region, line1: `${name} ${street}`, zip: postalCode })
		setDefaultSelectedCountry(isoCountryCode)
	}

	const setDefaultSelectedCountry = (code) => {
		const [country] = CountriesList.filter((country) => country.code == code)
		if (!country) return
		setSelectedCountry(country)
	}

	const handleSignUpPress = () => {
		if (isLoading) return

		if (name == '' || email == '' || phone == '') {
			Alert.alert('All fields are required!', 'Please complete the form before proceeding')
			return
		}

		if (!selectedCountry) {
			Alert.alert('Please select a country code')
			return
		}

		if (!address) {
			Alert.alert('Please provide your residential address')
			return
		}

		setIsLoading(true)
		axios
			.post('/guests/store-courier-registration', {
				name,
				email,
				phone: `${selectedCountry.dial_code}${phone}`,
				address,
			})
			.then(({ data }) => {
				setIsLoading(false)
				setApplicationCompleteModalVisible(true)
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response)
				if (err.response.status == 422) {
					setErrors(err.response.data.errors)
					return
				}
				Alert.alert(
					'An occured!',
					'An error occured while creating your account. Our engineers have been informed and the issue will be resolved shortly'
				)
			})
	}

	const handleOnAddressPress = (address) => {
		setAddressModalVisible(false)
		if (address.city == '') {
			Alert.alert('Could not determing the city of this location')
		} else {
			setAddress(address)
		}
	}

	return (
		<>
			<Content contentContainerStyle={{ alignItems: 'center' }}>
				<StatusBar barStyle={Layout.isIOS ? 'dark-content' : 'light-content'} />
				<TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
					<View
						style={{
							padding: 16,
							marginBottom: 10,
							width: Layout.window.width - 40,
							backgroundColor: 'white',
							borderRadius: 10,
							marginTop: 80,
							marginBottom: 80,
							...Layout.styles.elevationShadow(3),
						}}
					>
						<Image
							source={require('../../assets/images/app-icon-transparent.png')}
							style={{
								width: 100,
								height: 100,
								alignSelf: 'center',
								marginTop: -66,
							}}
						/>

						<Text style={{ fontSize: 30, fontWeight: 'bold', marginBottom: 10 }}>Sign Up</Text>

						<TextInput
							autoCapitalize={'words'}
							style={styles.inputBox}
							placeholder='Name'
							onChangeText={(text) => setName(text)}
							value={name}
							keyboardType='default'
						/>
						<Text style={styles.error}>{errors && errors.name && errors.name[0]}</Text>

						<TextInput
							autoCapitalize={'none'}
							style={styles.inputBox}
							placeholder='Email'
							onChangeText={(text) => setEmail(text)}
							value={email}
							keyboardType='email-address'
						/>
						<Text style={styles.error}>{errors && errors.email && errors.email[0]}</Text>

						{/* Phone number */}
						<View style={{ flexDirection: 'row', height: 50, marginVertical: 10 }}>
							<TouchableOpacity
								onPress={() => setCountriesModalVisible(true)}
								style={{
									backgroundColor: Colors.orange2[300],
									borderRadius: 5,
									marginRight: 10,
									padding: 10,
									...Layout.styles.centerRow,
								}}
							>
								<Text style={{ color: Colors.orange2[800] }}>{selectedCountry?.dial_code || 'Country'}</Text>
								<MaterialCommunityIcons name='menu-down' size={20} color={Colors.orange2[800]} />
							</TouchableOpacity>
							<TextInput
								autoCapitalize={'none'}
								style={{ backgroundColor: '#eee', height: 50, padding: 10, borderRadius: 5, flex: 1 }}
								placeholder='Phone'
								onChangeText={(text) => setPhone(text)}
								value={phone}
								keyboardType='phone-pad'
								textContentType='telephoneNumber'
							/>
							<Text style={styles.error}>{errors && errors.phone && errors.phone[0]}</Text>
						</View>

						{/* Address */}
						{/* <TouchableOpacity
							onPress={() => {
								setAddressModalVisible(true)
							}}
							style={{
								backgroundColor: '#eee',
								minHeight: 50,
								borderRadius: 5,
								padding: 10,
								justifyContent: 'center',
								marginVertical: 10,
							}}
						>
							<Text numberOfLines={2}>{address?.line1 || 'Your address...'}</Text>
						</TouchableOpacity> */}

						<TouchableOpacity
							style={{
								padding: 16,
								justifyContent: 'center',
								alignItems: 'center',
								backgroundColor: Colors.orange,
								borderRadius: 10,
								marginTop: 20,
							}}
							onPress={handleSignUpPress}
						>
							{isLoading ? (
								<ActivityIndicator color='white' />
							) : (
								<Text
									style={{
										fontWeight: 'bold',
										fontSize: 20,
										color: 'white',
									}}
								>
									Continue
								</Text>
							)}
						</TouchableOpacity>

						<Text style={{ marginTop: 20, alignSelf: 'center', padding: 10 }} onPress={() => navigation.navigate('Login')}>
							Already have an account? <Text style={{ fontWeight: 'bold' }}>Log In</Text>
						</Text>
					</View>
				</TouchableWithoutFeedback>
			</Content>

			<Modal
				isVisible={countriesModalVisible}
				onSwipeComplete={() => setCountriesModalVisible(false)}
				onBackdropPress={() => setCountriesModalVisible(false)}
				onBackButtonPress={() => setCountriesModalVisible(false)}
				swipeDirection='down'
				propagateSwipe
				style={{ margin: 0, justifyContent: 'flex-end' }}
			>
				<View style={{ height: Layout.window.height * 0.8, backgroundColor: 'white' }}>
					<View style={{ padding: 16, backgroundColor: 'black' }}>
						<Text style={{ fontSize: 20, color: Colors.black[200] }}>Select Country</Text>
					</View>
					<List
						dataArray={CountriesList}
						keyExtractor={(item) => item.code}
						renderRow={(country) => (
							<ListItem
								selected
								onPress={() => {
									setSelectedCountry(country)
									setCountriesModalVisible(false)
								}}
							>
								<Body>
									<Text>{country.name}</Text>
								</Body>
								<Right>
									<Text style={{ color: 'gray' }}>({country.dial_code})</Text>
								</Right>
							</ListItem>
						)}
					/>
				</View>
			</Modal>

			<Modal
				isVisible={addressModalVisible}
				onSwipeComplete={() => setAddressModalVisible(false)}
				onBackdropPress={() => setAddressModalVisible(false)}
				onBackButtonPress={() => setAddressModalVisible(false)}
				swipeDirection='down'
				avoidKeyboard={false}
				coverScreen={false}
				style={{ margin: 0, justifyContent: 'flex-end' }}
				onModalShow={() => addressInputRef.current.triggerFocus()}
				onModalWillHide={() => addressInputRef.current.triggerBlur()}
			>
				{/* <KeyboardAvoidingView behavior='padding' enabled> */}
				<View style={{ height: Layout.window.height * 0.9, backgroundColor: 'white' }}>
					<View style={{ padding: 16, backgroundColor: 'black' }}>
						<Text style={{ fontSize: 20, color: Colors.black[200] }}>Your residential address</Text>
					</View>
					<View style={{ padding: 16, borderWidth: 6, borderColor: 'red', flex: 1 }}>
						{/* <Content> */}
						<GooglePlacesAutocomplete
							ref={addressInputRef}
							onFocus={() => null} // HACK: I modified this package's source code to trigger this method on line 627.... also disabled hiding of listView when the input blurs
							placeholder='Start typing an address...'
							minLength={2} // minimum length of text to search
							returnKeyType={'search'}
							fetchDetails={true}
							renderDescription={(row) => (
								<>
									<FeatherIcons name={'map-pin'} size={18} color={Colors.muted} />
									<Text> </Text>
									<Text style={{ paddingLeft: 10 }}>{row.description}</Text>
								</>
							)}
							onPress={(data, details = null) => handleOnAddressPress(extractAddressComponents(details))}
							getDefaultValue={() => address?.line1 || ' '}
							query={{ key: Config.google.MAPS_APIKEY, language: 'en' }}
							styles={autocompleteStyles}
							debounce={200}
						/>
						{/* </Content> */}
					</View>
				</View>
				{/* </KeyboardAvoidingView> */}
			</Modal>

			<Modal
				isVisible={applicationCompleteModalVisible}
				onSwipeComplete={() => setApplicationCompleteModalVisible(false)}
				onBackdropPress={() => setApplicationCompleteModalVisible(false)}
				onBackButtonPress={() => setApplicationCompleteModalVisible(false)}
				onModalHide={() => navigation.goBack()}
				swipeDirection='down'
			>
				<View style={{ ...Layout.styles.center, backgroundColor: 'white', borderRadius: 8, padding: 16 }}>
					<MaterialCommunityIcons name='checkbox-marked-circle-outline' size={50} color='green' />
					<Text style={{ fontSize: 25, marginBottom: 10, textAlign: 'center' }}>Your application has been submitted!</Text>
					<Text style={{ fontSize: 18, marginBottom: 10, textAlign: 'center' }}>
						Our team will review your application and contact you shortly with the next steps.
					</Text>
					<Text style={{ fontSize: 18, marginBottom: 20, textAlign: 'center' }}>Thank you for choosing SweeterFix.</Text>
					<TouchableOpacity
						onPress={() => setApplicationCompleteModalVisible(false)}
						style={{ padding: 16, backgroundColor: 'black', borderRadius: 5 }}
					>
						<Text style={{ color: 'white', fontSize: 20, width: 100, textAlign: 'center' }}>OK</Text>
					</TouchableOpacity>
				</View>
			</Modal>
		</>
	)
}

const styles = StyleSheet.create({
	inputBox: {
		backgroundColor: '#eee',
		height: 50,
		padding: 10,
		marginVertical: 10,
		borderRadius: 5,
	},
	error: {
		marginTop: -10,
		color: 'red',
	},
})

const autocompleteStyles = StyleSheet.create({
	textInputContainer: {
		width: '100%',
		height: 55,
		borderTopWidth: 0,
		borderBottomWidth: 0,
		backgroundColor: 'white',
	},
	textInput: { backgroundColor: '#eee', height: 50, padding: 10, marginLeft: 0, marginRight: 0, marginTop: 10, marginBottom: 10 },
	description: {
		fontWeight: '200',
		fontSize: 18,
		// paddingLeft: 30
	},
	predefinedPlacesDescription: {
		color: '#1faadb',
	},
	row: {
		height: 50,
	},
})
