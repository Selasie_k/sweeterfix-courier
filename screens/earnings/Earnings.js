import React, { useState, useEffect, useCallback } from 'react'
import { View, Text, RefreshControl, TouchableOpacity, ScrollView } from 'react-native'
import AppContext from '../../context/app-context'
import { Feather, Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'
import Layout from '../../constants/Layout'
import Colors from '../../constants/Colors'
import axios from 'axios'
import moment from 'moment'

export default ({ navigation }) => {
	const [isLoading, setIsLoading] = useState(false)
	const [jobs, setJobs] = useState({})

	useEffect(() => {
		loadDataAsync()
	}, [])

	const loadDataAsync = () => {
		setIsLoading(true)
		axios
			.get('/courier/earnings')
			.then(({ data }) => {
				setJobs(data)
				// const [key, jobs] = Object.entries(data)
				// console.log(data)
				setIsLoading(false)
			})
			.catch((err) => {
				setIsLoading(false)
				console.log(err.response)
			})
	}

	const renderEmptyData = () => (
		<View style={{ flex: 1, ...Layout.styles.center, padding: 20 }}>
			<Ionicons name='ios-wallet' size={100} color={Colors.black[400]} />
			<Text style={{ fontSize: 20, textAlign: 'center' }}>No earnings yet</Text>
			<TouchableOpacity onPress={loadDataAsync}>
				<Text style={{ marginTop: 20, color: Colors.green[600], fontSize: 15 }}>REFRESH</Text>
			</TouchableOpacity>
		</View>
	)

	if (!isLoading && Object.keys(jobs).length < 1) return renderEmptyData()

	return (
		<>
			<View style={{ flexDirection: 'row', padding: 16, backgroundColor: Colors.black[900], justifyContent: 'space-between' }}>
				<Text style={{ fontSize: 18, color: Colors.black[300] }}>Total Earnings</Text>
				<Text style={{ fontSize: 18, color: Colors.green[500] }}>{totalEarnings(jobs)}</Text>
			</View>
			<ScrollView refreshControl={<RefreshControl refreshing={isLoading} onRefresh={loadDataAsync} />} style={{ flex: 1 }}>
				{Object.entries(jobs).map(([date, jobs]) => (
					<View key={date}>
						<View
							style={{
								paddingHorizontal: 16,
								paddingTop: 25,
								paddingBottom: 14,
								flexDirection: 'row',
								justifyContent: 'space-between',
							}}
						>
							<Text style={{ fontSize: 16, color: Colors.black[500] }}>{moment.utc(date).local().format('dddd, MMM D YYYY')}</Text>
							<Text style={{ fontSize: 16, color: 'green' }}>
								{jobs[0].currency_symbol} {daysEarning(jobs)}
							</Text>
						</View>
						{jobs.map((job, index) => (
							<TouchableOpacity
								onPress={() => navigation.navigate('Earning', { job })}
								key={job.id.toString()}
								style={{ marginBottom: 1, backgroundColor: 'white', flexDirection: 'row' }}
							>
								<View style={{ flex: 1, padding: 16, flexDirection: 'row' }}>
									<View style={{ flex: 1 }}>
										<Text style={{ fontSize: 14, color: Colors.black[500], marginBottom: 5 }}>Order #{job.order.number}</Text>
										<Text style={{ ...Layout.styles.text[100], marginBottom: 4 }}>{job.pickup_name}</Text>
										<Text style={{ ...Layout.styles.text[200], marginBottom: 8 }} numberOfLines={1}>
											{job.pickup_address}
										</Text>
										{job.order.courier_transfer_id ? (
											<Text style={{ fontSize: 14, color: Colors.green[700] }}>Paid out</Text>
										) : (
											<Text style={{ fontSize: 14, color: Colors.red[700] }}>Pending payout</Text>
										)}
									</View>
									<View style={{ justifyContent: 'space-between', alignItems: 'flex-end' }}>
										<Text style={{ fontSize: 14, color: Colors.black[500] }}>
											{moment.utc(job.completed_at).local().format('hh:mm A')}
										</Text>
										<Text style={{ fontSize: 15 }}>
											{job.currency_symbol} {(job.earning / 100).toFixed(2)}
										</Text>
									</View>
								</View>
								{/* <View style={{ ...Layout.styles.center }}>
								<Feather name='chevron-right' size={20} color={Colors.black[600]} />
							</View> */}
							</TouchableOpacity>
						))}
					</View>
				))}
			</ScrollView>
		</>
	)
}

const daysEarning = (jobs) => {
	let total = jobs.reduce((acc, job) => {
		return acc + job.earning
	}, 0)
	return (total / 100).toFixed(2)
}

const totalEarnings = (data) => {
	if (Object.keys(data).length < 1) return

	const _data = Object.values(data)
	const currency = _data[0][0].currency_symbol

	const total = _data.reduce((acc1, jobs) => {
		return (
			acc1 +
			jobs.reduce((acc2, job) => {
				return acc2 + job.earning
			}, 0)
		)
	}, 0)
	return `${currency} ${(total / 100).toFixed(2)}`
}
