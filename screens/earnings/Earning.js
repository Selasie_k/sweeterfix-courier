import React, { useState, useEffect, useContext } from 'react'
import { View, Text, Button } from 'react-native'
import { Content } from 'native-base'
import moment from 'moment'

export default ({ navigation, route }) => {
	const [job, setJob] = useState({})
	useEffect(() => {
		setJob(route.params.job)
		// console.log(route.params.job)
	}, [])

	return (
		<Content style={{ padding: 16 }}>
			<View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 30 }}>
				<Text style={{ fontSize: 20, color: 'green' }}>
					{job.currency_symbol} {(job.earning / 100).toFixed(2)}
				</Text>
				<Text style={{ fontSize: 18 }}>{moment(job.completed_at).format('Do MMMM YYYY')}</Text>
			</View>
			<View style={{ flexDirection: 'row' }}>
				<View style={{ alignItems: 'center', width: 30 }}>
					<View style={{ height: 15, width: 15, backgroundColor: 'black' }} />
					<View style={{ height: 100, width: 2, backgroundColor: 'black' }} />
				</View>
				<View style={{ paddingHorizontal: 12, flex: 1 }}>
					<Text style={{ fontSize: 18, marginBottom: 5 }}>{job?.pickup_name}</Text>
					<Text style={{ fontSize: 14, marginBottom: 5 }}>{job.pickup_address}</Text>
					<Text style={{ fontSize: 13, marginBottom: 5, textAlign: 'right', color: 'gray' }}>
						{moment.utc(job.pickup_trip_started_at).local().format('h:mm A')}
					</Text>
				</View>
			</View>
			<View style={{ flexDirection: 'row' }}>
				<View style={{ alignItems: 'center', width: 30 }}>
					<View style={{ height: 15, width: 15, backgroundColor: 'black' }} />
				</View>
				<View style={{ paddingHorizontal: 12, flex: 1 }}>
					<Text style={{ fontSize: 18, marginBottom: 5 }}>{job.dropoff_name}</Text>
					<Text style={{ fontSize: 14, marginBottom: 5 }}>{job.dropoff_address}</Text>
					<Text style={{ fontSize: 13, marginBottom: 5, alignSelf: 'flex-end', color: 'gray' }}>
						{moment.utc(job.completed_at).local().format('h:mm A')}
					</Text>
				</View>
			</View>
		</Content>
	)
}
