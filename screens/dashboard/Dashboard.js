import React, { useEffect, useState, useRef } from 'react'
import {
	View,
	Text,
	ActivityIndicator,
	StyleSheet,
	AppState,
	TouchableWithoutFeedback,
	Button,
	TouchableOpacity,
	StatusBar,
	Alert,
} from 'react-native'
import { Linking } from 'expo'
import MapView, { Marker } from 'react-native-maps'
import { useFocusEffect } from '@react-navigation/native'
import Layout from '../../constants/Layout'
import MapStyle from '../../constants/MapStyle'
import Config from '../../config/app'
import * as Permissions from 'expo-permissions'
import * as Location from 'expo-location'
import * as IntentLauncher from 'expo-intent-launcher'
import AppContext from '../../context/app-context'
import JobCard from './JobCard'
import JobInProgressModal from './JobInProgressModal'
import { Feather, FontAwesome, MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons'
import ActionSheet from 'react-native-actionsheet'
import axios from 'axios'
import MapViewDirections from 'react-native-maps-directions'

export default ({ navigation }) => {
	const mapRef = React.useRef()
	const navigationMarkerRef = React.useRef()

	const [navigationRefreshInterval, setNavigationRefreshInterval] = useState(null)

	const {
		isLocationPermissionGranted,
		setIsLocationPermissionGranted,
		currentCoords,
		setCurrentCoords,
		jobAccepted,
		setJobAccepted,
		jobReceived,
		setJobReceived,
		user,
		order,
		dropOffCoords,
		pickupCoords,
		setPickupRouteParams,
		setdropOffRouteParams,
		setCurrentJobLeg,
		setOrder,
		navigationInProgress,
		setNavigationInProgress,
		accessingUserLocation,
		setAccessingUserLocation,
	} = React.useContext(AppContext)

	const [appState, setAppState] = useState('')

	const [jobInProgressModalVisible, setJobInProgressModalVisible] = useState(false)

	// const [accessingUserLocation, setAccessingUserLocation] = useState(null)

	const [barStyle, setBarStyle] = useState('light-content')

	useEffect(() => {
		getUserPermissionsAsync()
		AppState.addEventListener('change', _handleAppStateChange)
		return () => {
			AppState.removeEventListener('change', _handleAppStateChange)
		}
	}, [])

	useFocusEffect(
		React.useCallback(() => {
			setBarStyle('light-content')
			return () => {
				if (Layout.isIOS) {
					setBarStyle('dark-content')
				}
			}
		})
	)

	useEffect(() => {
		if (jobReceived) {
			animateMapToTargetMarkers()
		} else {
			animateMapToUserLocation(currentCoords)
		}
	}, [jobReceived])

	const animateMapToTargetMarkers = () => {
		mapRef.current.fitToSuppliedMarkers(['PickUpMarker', 'CourierMarker', 'DropOffMarker'], {
			edgePadding: { bottom: Layout.window.height / 2, left: 20, right: 20, top: Layout.statusBarHeight + 20 },
			animated: true,
		})
	}

	const animateMapToUserLocation = ({ longitude, latitude }) => {
		mapRef.current.animateCamera({ center: { longitude, latitude }, pitch: 10, zoom: 16 }, { duration: 500 })
	}

	const _handleAppStateChange = (nextAppState) => {
		if (appState.match(/inactive|background/) && nextAppState === 'active') {
			getUserPermissionsAsync()
		}
		setAppState(nextAppState)
	}

	const getUserPermissionsAsync = async () => {
		const status = await Permissions.askAsync(Permissions.NOTIFICATIONS, Permissions.LOCATION)
		setIsLocationPermissionGranted(status.permissions.location.granted)
		if (status.permissions.location.granted) {
			getLocationAsync()
		}
	}

	const openSettingsAsync = () => {
		if (Layout.isAndroid) {
			IntentLauncher.startActivityAsync(IntentLauncher.ACTION_LOCATION_SOURCE_SETTINGS)
		} else {
			Linking.openURL('app-settings:')
		}
	}

	const getLocationAsync = async () => {
		setAccessingUserLocation(true)
		// const location = await Location.getCurrentPositionAsync({
		const location = await Location.getLastKnownPositionAsync({
			// enableHighAccuracy: false,
			accuracy: Location.Accuracy.Balanced
		})
		// console.log(location)
		const { longitude, latitude, heading } = location.coords
		setCurrentCoords({
			latitude,
			longitude,
			heading,
			latitudeDelta: 0.008,
			longitudeDelta: 0.008,
		})
		setAccessingUserLocation(false)
		animateMapToUserLocation(location.coords)
	}

	const handleNavigationStart = () => {
		// do an async call to alert server
		setJobInProgressModalVisible(false)
		setNavigationInProgress(true)
		var interval = navigator.geolocation.watchPosition(
			(location) => {
				// console.log(location)
				// navigationMarkerRef.current.redraw()
				// mapRef.current.animateCamera({ center: location.coords, pitch: 100, heading: location.coords.heading, zoom: 20 }, { duration: 500 })
			},
			null,
			{
				distanceFilter: 0,
				enableHighAccuracy: true,
			}
		)
		setNavigationRefreshInterval(interval)
	}

	const cancelJobAsync = () => {
		setJobAccepted(false)
		setJobReceived(false)
		setJobInProgressModalVisible(false)
		setOrder(false)

		navigator.geolocation.stopObserving(navigationRefreshInterval)
	}

	return (
		<View style={{ flex: 1 }}>
			<StatusBar barStyle={barStyle} />
			<MapView
				style={{ ...StyleSheet.absoluteFillObject }}
				customMapStyle={MapStyle}
				ref={mapRef}
				provider='google'
				initialRegion={currentCoords}
				showsUserLocation
				showsCompass
				showsPointsOfInterest={false}
				// showsTraffic
				loadingEnabled
			>
				{jobReceived && pickupCoords && dropOffCoords && (
					<>
						<Marker identifier='PickUpMarker' title='Pick up' coordinate={pickupCoords}>
							{/* <FontAwesome name='map-marker' size={40} /> */}
							<MaterialCommunityIcons name='alpha-a-box' size={40} />
						</Marker>
						<Marker identifier='DropOffMarker' title='Drop off' coordinate={dropOffCoords}>
							{/* <MaterialIcons name='person-pin-circle' size={40} color={'green'} /> */}
							<MaterialCommunityIcons name='alpha-b-box' size={40} />
						</Marker>
						<Marker identifier='CourierMarker' title='You' coordinate={currentCoords}>
							{/* <MaterialIcons name='person-pin-circle' size={40} /> */}
							<View></View>
						</Marker>
						{/* pickup route */}
						<MapViewDirections
							origin={currentCoords}
							destination={pickupCoords}
							apikey={Config.google.MAPS_APIKEY}
							strokeWidth={4}
							strokeColor='gray'
							onReady={(route) => setPickupRouteParams(route)}
						/>
						{/* drop off route */}
						<MapViewDirections
							origin={pickupCoords}
							destination={dropOffCoords}
							apikey={Config.google.MAPS_APIKEY}
							strokeWidth={4}
							strokeColor='black'
							onReady={(route) => setdropOffRouteParams(route)}
						/>

						{/* {navigationInProgress && (
							<Marker
								ref={navigationMarkerRef}
								identifier='CourierMarker'
								flat
								rotation={currentCoords.heading}
								coordinate={currentCoords}
							>
								<MaterialIcons name='navigation' size={80} />
							</Marker>
						)} */}
					</>
				)}
			</MapView>

			{!isLocationPermissionGranted ? (
				<EnableLocationButton openSettingsAsync={openSettingsAsync} getUserPermissionsAsync={getUserPermissionsAsync} />
			) : accessingUserLocation ? (
				<LoadingLocation />
			) : (
				<>
					{jobReceived && (
						<JobCard
							jobReceived={jobReceived}
							setJobReceived={setJobReceived}
							setJobInProgressModalVisible={setJobInProgressModalVisible}
						/>
					)}

					{jobAccepted && (
						<>
							<JobInProgressCardButton onPress={() => setJobInProgressModalVisible(true)} />
							<JobInProgressModal
								jobInProgressModalVisible={jobInProgressModalVisible}
								setJobInProgressModalVisible={setJobInProgressModalVisible}
								handleNavigationStart={handleNavigationStart}
								cancelJobAsync={cancelJobAsync}
							/>
						</>
					)}

					{!(jobReceived || jobAccepted) && <GoOnlineButton />}

					{!user.isAvailable ? <OfflineOverlay /> : null}

					{/* {order && (
						
					)} */}
				</>
			)}
		</View>
	)
}

const OfflineOverlay = () => {
	return (
		<View
			style={{
				...StyleSheet.absoluteFillObject,
				height: Layout.window.height,
				backgroundColor: 'black',
				opacity: 0.7,
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			<Text style={{ color: 'white', fontSize: 30, fontWeight: 'bold', textAlign: 'center' }}>You're currently not accepting jobs</Text>
		</View>
	)
}

const JobInProgressCardButton = ({ onPress }) => {
	return (
		<View
			style={{
				position: 'absolute',
				bottom: 0,
				left: 0,
				right: 0,
				backgroundColor: 'black',
			}}
		>
			<TouchableOpacity onPress={onPress}>
				<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 16 }}>
					<View>
						<FontAwesome name='circle' color='green' size={16} style={{ marginRight: 5 }} />
					</View>
					<View style={{ flex: 1 }}>
						<Text numberOfLines={1} style={{ color: 'white' }}>
							Heading to pick up from Crema Pastry at 122 West Ave, Kitchener, ON N2M 1X7, Canada
						</Text>
					</View>
					<View>
						<Feather name='chevron-up' color='white' size={25} />
					</View>
				</View>
			</TouchableOpacity>
		</View>
	)
}

const LoadingLocation = () => {
	return (
		<View
			style={{
				...StyleSheet.absoluteFillObject,
				height: Layout.window.height,
				backgroundColor: 'black',
				opacity: 0.7,
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			<ActivityIndicator color='white' />
			<Text style={{ color: 'white', fontSize: 20 }}>Resolving your location...</Text>
		</View>
	)
}

const EnableLocationButton = ({ openSettingsAsync, getUserPermissionsAsync }) => {
	return (
		<View
			style={{
				...StyleSheet.absoluteFillObject,
				height: Layout.window.height,
				backgroundColor: 'black',
				opacity: 0.7,
				justifyContent: 'center',
				alignItems: 'center',
			}}
		>
			<Text style={{ color: 'white', fontSize: 20 }} onPress={openSettingsAsync}>
				Enable Location Access
			</Text>
			<Button title='REFRESH' color='white' onPress={getUserPermissionsAsync} />
		</View>
	)
}

const GoOnlineButton = () => {
	const onlineStatusActionSheet = React.useRef()
	const { user, setUser, currentCoords, setCurrentCoords } = React.useContext(AppContext)
	const [isLoading, setIsLoading] = useState(false)
	const [positionWatcher, setPositionWatcher] = useState(null)

	const _handleOnlineStatusActionSheetOptionPress = (index) => {
		const actions = [() => (user.isAvailable ? goOfflineAsync() : goOnlineAsync())]
		actions[index] && actions[index]()
	}

	// const _handleOnlineStatusActionSheetOptionPress = (index) => {
	// 	const actions = [() => _changeOnlineStatusAsync()]
	// 	actions[index] && actions[index]()
	// }

	// const _changeOnlineStatusAsync = async () => {
	// 	const position = await Location.getLastKnownPositionAsync()
	// 	console.log(position)
	// 	return
	// 	if (isLoading) return
	// 	setIsLoading(true)
	// 	axios
	// 		.patch(`/courier/update-online-status`, { status: !user.isAvailable })
	// 		.then(({ data: { user } }) => {
	// 			setUser(user)
	// 			setIsLoading(false)
	// 		})
	// 		.catch((err) => {
	// 			setIsLoading(false)
	// 			console.log(err.response)
	// 		})
	// }

	const goOnlineAsync = async () => {
		if (isLoading) return
		setIsLoading(true)
		const {
			coords: { latitude, longitude },
		} = await Location.getLastKnownPositionAsync()
		setCurrentCoords({ ...currentCoords, latitude, longitude })
		try {
			const { data } = await axios.patch('/courier/go-online', { position: { latitude, longitude } })
			data.user && setUser(data.user)
			watchPositionAsync()
		} catch (error) {
			console.log(error.response.data)
			Alert.alert('Something went wrong', 'Check your internet connection and try again.')
		}
		setIsLoading(false)
	}

	const goOfflineAsync = async () => {
		if (isLoading) return
		setIsLoading(true)
		try {
			const { data } = await axios.patch('/courier/go-offline')
			data.user && setUser(data.user)
			positionWatcher && positionWatcher.remove()
		} catch (error) {
			console.log(error.response.data)
			Alert.alert('Something went wrong', 'Check your internet connection and try again.')
		}
		setIsLoading(false)
	}

	const watchPositionAsync = async () => {
		const watcher = await Location.watchPositionAsync(
			{
				accuracy: Location.Accuracy.High,
				distanceInterval: 20,
			},
			async ({ coords: { latitude, longitude } }) => {
				setCurrentCoords({ ...currentCoords, latitude, longitude })
				try {
					await axios.patch('/courier/set-latest-position', { position: { latitude, longitude } })
				} catch (error) {
					console.log(error.response.data)
				}
			}
		)
		setPositionWatcher(watcher)
	}

	return (
		<>
			<TouchableWithoutFeedback onPress={() => onlineStatusActionSheet.current.show()}>
				<View
					style={{
						position: 'absolute',
						bottom: 30,
						padding: 15,
						right: 80,
						left: 80,
						borderRadius: 25,
						backgroundColor: user.isAvailable ? 'gray' : 'green',
						alignItems: 'center',
						zIndex: 100,
						...Layout.styles.elevationShadow(3),
					}}
				>
					{isLoading ? (
						<ActivityIndicator color='white' />
					) : (
						<Text style={{ color: 'white', fontSize: 20 }}>Go {user.isAvailable ? 'Offline' : 'Online'}</Text>
					)}
				</View>
			</TouchableWithoutFeedback>

			<ActionSheet
				ref={onlineStatusActionSheet}
				options={[user.isAvailable ? 'Stop Accepting Jobs' : 'Start Accepting Jobs', 'Cancel']}
				cancelButtonIndex={1}
				destructiveButtonIndex={user.isAvailable ? 0 : null}
				onPress={(index) => _handleOnlineStatusActionSheetOptionPress(index)}
			/>
		</>
	)
}
