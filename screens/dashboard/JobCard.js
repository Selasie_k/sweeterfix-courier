import React, { useEffect, useState, useRef } from 'react'
import { View, Text, Vibration, TouchableOpacity, Animated, Alert } from 'react-native'
import Layout from '../../constants/Layout'
import AppContext from '../../context/app-context'
import moment from 'moment'

export default ({ setJobInProgressModalVisible }) => {
	const {
		jobReceived,
		setJobReceived,
		setJobAccepted,
		order,
		setOrder,
		pickupRouteParams,
		dropOffRouteParams,
		currentJobLeg,
		pickupCoords,
		setCurrentJobLeg
	} = React.useContext(AppContext)
	const [counter, setCounter] = useState(9)
	const [startCounter, setStartCounter] = useState(false)
	const PATTERN = [1000, 1000]
	const [jobCardTranslateY] = useState(new Animated.Value(1000))

	useEffect(() => {
		if (jobReceived) {
			showJob()
		}
	}, [jobReceived])

	useEffect(() => {
		const timer = counter > 0 && startCounter && setInterval(() => setCounter(counter - 1), 1000)
		if (counter < 1) stopTimer()
		return () => clearInterval(timer)
	}, [counter, startCounter])

	const startTimer = () => {
		setCounter(10)
		setStartCounter(true)
		Vibration.vibrate(PATTERN, true)
	}

	const stopTimer = () => {
		setStartCounter(false)
		// setJobReceived(false)
		Vibration.cancel()
	}

	const dismissJobCard = () => {
		// emmit an event to parent here
		stopTimer()
		Animated.timing(jobCardTranslateY, {
			toValue: 1000,
			duration: 200
		}).start()
	}

	const showJob = () => {
		startTimer()
		Animated.timing(jobCardTranslateY, {
			toValue: 0,
			duration: 200
		}).start()
	}

	const handleAcceptJob = () => {
		setCurrentJobLeg({
			type: 'PICK UP',
			destination: { pickupCoords },
			routeParams: pickupRouteParams,
			entity: order.shop,
			isPickup: true
		})
		dismissJobCard()
		setJobAccepted(true)
		setJobInProgressModalVisible(true)
	}

	const handleDeclineJob = () => {
		Alert.alert('ARE YOU SURE YOU WANT TO DECLINE?', "This impacts your ratings. If you're not available for dispatches, simply go offline.", [
			{ text: 'Yes, Decline!', onPress: () => declineJobAsync(), style: 'destructive' },
			{ text: 'Cancel', style: 'cancel' }
		])
	}

	const declineJobAsync = order => {
		dismissJobCard()
		setJobReceived(false)
		setOrder(false)
		// axios.patch(`/courier/decline/${order.id}`)
		// .then(res => {
		// //   this.context.removeOrder(order)
		//   alert('Order declined')
		// })
		// .catch(err => console.log(err.response))
	}

	return (
		<Animated.View
			style={{
				position: 'absolute',
				...Layout.styles.elevationShadow(3),
				bottom: 20,
				left: 20,
				right: 20,
				borderRadius: 10,
				backgroundColor: 'white',
				padding: 12,
				transform: [{ translateY: jobCardTranslateY }]
			}}
		>
			{/* counter bubble */}
			{counter > 0 && (
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', ...Layout.styles.elevationShadow(3) }}>
					<View
						style={{
							backgroundColor: 'white',
							justifyContent: 'center',
							alignItems: 'center',
							height: 80,
							width: 80,
							borderRadius: 40,
							marginTop: -50
						}}
					>
						<Text style={{ color: 'red', fontSize: 40 }}>{counter}</Text>
					</View>
				</View>
			)}

			{/* Upper Section */}
			<View style={{ flex: 2, flexDirection: 'row' }}>
				{/* Top Left */}
				<View style={{ flex: 1, padding: 12 }}>
					<Text style={Layout.styles.text[300]}>PICK UP</Text>
					<Text style={Layout.styles.text[100]}>{order.shop.address}</Text>
				</View>
				{/* Top Right */}
				<View style={{ flex: 1, padding: 12 }}>
					<Text style={Layout.styles.text[300]}>DROP OFF</Text>
					<Text style={Layout.styles.text[100]}>{order.shipping.line1}</Text>
				</View>
			</View>

			{/* Middle Section */}
			<View style={{ flex: 2, flexDirection: 'row' }}>
				{/* Mid Left */}
				<View style={{ flex: 1, padding: 12 }}>
					<Text style={Layout.styles.text[300]}>JOB DURATION (Est)</Text>
					<Text style={Layout.styles.text[100]}>{parseInt(pickupRouteParams?.duration + dropOffRouteParams?.duration)} mins</Text>
				</View>
				{/* Mid Right */}
				<View style={{ flex: 1, padding: 12 }}>
					<Text style={Layout.styles.text[300]}>DROP OFF BY</Text>
					<Text style={Layout.styles.text[100]}>
						{moment(order.shipping.time.from, 'HH:mm a').format('h:mma')} - {moment(order.shipping.time.to, 'HH:mm a').format('h:mma')}
					</Text>
				</View>
			</View>

			{/* Lower Section */}
			<View style={{ flex: 2, flexDirection: 'row' }}>
				{/* Bottom Left */}
				<View style={{ flex: 1, padding: 12 }}>
					<Text style={Layout.styles.text[300]}>TOT. DISTANCE (Est)</Text>
					<Text style={Layout.styles.text[100]}>
						{parseFloat(pickupRouteParams?.distance + dropOffRouteParams?.distance).toFixed(1)} km
					</Text>
				</View>

				{/* Bottom Right */}
				<View style={{ flex: 1, padding: 12 }}>
					<Text style={Layout.styles.text[300]}>EARNING</Text>
					<Text style={Layout.styles.text[100]}>
						{order.shop.currency_symbol} {order.dispatch_commission / 100}
					</Text>
				</View>
			</View>

			{/* Accept / Decline Buttons */}
			<View style={{ flex: 1, flexDirection: 'row', padding: 12 }}>
				<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
					<TouchableOpacity onPress={handleDeclineJob}>
						<Text style={{ color: 'red', padding: 10 }}>Decline</Text>
					</TouchableOpacity>
				</View>
				<View style={{ flex: 1 }}>
					<TouchableOpacity onPress={handleAcceptJob}>
						<View
							style={{
								flex: 1,
								justifyContent: 'center',
								alignItems: 'center',
								padding: 10,
								backgroundColor: 'green',
								borderRadius: 30
							}}
						>
							<Text style={{ color: 'white', fontWeight: 'bold' }}>Accept Job</Text>
						</View>
					</TouchableOpacity>
				</View>
			</View>
		</Animated.View>
	)
}
