import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, SafeAreaView } from 'react-native'
import Layout from '../../constants/Layout'
import Modal from 'react-native-modal'
import Colors from '../../constants/Colors'
import { Feather, MaterialIcons } from '@expo/vector-icons'
import AppContext from '../../context/app-context'

export default ({ jobInProgressModalVisible, setJobInProgressModalVisible, handleNavigationStart, cancelJobAsync }) => {
	const { setJobAccepted, order, setOrder, setJobReceived, dropOffRouteParams, dropOffCoords, currentJobLeg, setCurrentJobLeg } = React.useContext(
		AppContext
	)

	useEffect(() => {}, [])

	// const handleNavigationStart = () => {
	// 	// setCurrentJobLeg({
	// 	// 	type: 'DROP OFF',
	// 	// 	destination: { dropOffCoords },
	// 	// 	routeParams: dropOffRouteParams,
	// 	// 	entity: order.shipping,
	// 	// 	isPickup: false
	// 	// })
	// }

	return (
		<Modal
			animationIn='slideInUp'
			style={{ margin: 0, justifyContent: 'flex-end' }}
			isVisible={(currentJobLeg && jobInProgressModalVisible) || false}
			transparent
			onBackButtonPress={() => setJobInProgressModalVisible(false)}
			onBackdropPress={() => setJobInProgressModalVisible(false)}
			onRequestClose={() => setJobInProgressModalVisible(false)}
			onSwipeComplete={() => setJobInProgressModalVisible(false)}
			hideModalContentWhileAnimating
			swipeDirection='down'
			onSwipeComplete={() => setJobInProgressModalVisible(false)}
		>
			<SafeAreaView style={{ height: Layout.window.height * 0.5, backgroundColor: 'white' }}>
				<View style={{ flex: 1 }}>
					<View style={{ padding: 16 }}>
						<Text style={Layout.styles.text[200]}>{currentJobLeg.type}</Text>
						<Text style={Layout.styles.text[100]}>{currentJobLeg.entity.name}</Text>
						<Text style={{ ...Layout.styles.text[200], color: 'black' }}>
							{currentJobLeg.isPickup ? currentJobLeg.entity.address : currentJobLeg.entity.line1}
						</Text>
					</View>
					<View style={{ flexDirection: 'row', backgroundColor: '#eeee' }}>
						{/* <View style={{ padding: 16 }}>
							<Text style={Layout.styles.text[200]}>ORDER REF</Text>
							<Text style={Layout.styles.text[100]}>3432-223423</Text>
						</View> */}
						<View style={{ flex: 1, padding: 16 }}>
							<Text style={Layout.styles.text[200]}>EST. DISTANCE</Text>
							<Text style={Layout.styles.text[100]}>{parseFloat(currentJobLeg.routeParams.distance).toFixed(1)} km</Text>
						</View>
						<View style={{ flex: 1, padding: 16 }}>
							<Text style={Layout.styles.text[200]}>ETA</Text>
							<Text style={Layout.styles.text[100]}>{parseInt(currentJobLeg.routeParams.duration)} mins</Text>
						</View>
					</View>
					{/* <View style={{ height: 1, backgroundColor: '#adadad' }} /> */}
					<View style={{ flex: 1, flexDirection: 'row' }}>
						<View style={{ flex: 1, ...Layout.styles.centerRow }}>
							<TouchableOpacity>
								<View style={Layout.styles.center}>
									<Feather name='phone-call' size={30} color='green' />
									<Text style={{ fontSize: 20, color: 'green', marginTop: 5 }}>Call</Text>
								</View>
							</TouchableOpacity>
						</View>
						<View style={{ flex: 1, ...Layout.styles.centerRow }}>
							<View style={{ padding: 16 }}>
								<Text style={Layout.styles.text[200]}>ORDER REF</Text>
								<Text style={{ fontSize: 30 }}>{order.number}</Text>
							</View>
						</View>
					</View>
				</View>
				<View style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: 16 }}>
					<TouchableOpacity>
						<View style={{ padding: 16, marginRight: 20 }}>
							<Text style={{ color: 'red', fontSize: 15 }} onPress={cancelJobAsync}>
								Cancel Job
							</Text>
						</View>
					</TouchableOpacity>
					<TouchableOpacity onPress={handleNavigationStart}>
						<View style={{ padding: 10, marginRight: 20, borderWidth: 1, ...Layout.styles.centerRow, backgroundColor: 'black' }}>
							<MaterialIcons name='navigation' size={20} color='white' />
							<Text style={{ fontSize: 15, color: 'white' }} onPress={null}>
								Start Navigation
							</Text>
						</View>
					</TouchableOpacity>
				</View>
			</SafeAreaView>
		</Modal>
	)
}
