import * as React from 'react'
import { Platform, StatusBar, StyleSheet, View, AsyncStorage } from 'react-native'
import { SplashScreen } from 'expo'
import { Asset } from 'expo-asset'
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import axios from 'axios'

import Config from './config/app'

import AuthLoadingScreen from './screens/auth/AuthLoadingAndNavigation'
import LoginScreen from './screens/auth/Login'
import RegisterScreen from './screens/auth/Register'

import AppContextProvider from './context/AppContextProvider'
import AppContext from './context/app-context'

import BottomTabNavigator from './navigation/BottomTabNavigator'
import useLinking from './navigation/useLinking'

const Stack = createStackNavigator()

export default function App(props) {
	const [isLoadingComplete, setLoadingComplete] = React.useState(false)
	const [initialNavigationState, setInitialNavigationState] = React.useState()
	const context = React.useContext(AppContext)

	const containerRef = React.useRef()
	const { getInitialState } = useLinking(containerRef)

	const setDefaultAxiosHeaders = () => {
		axios.defaults.baseURL = Config.http.baseUrl
		axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'
		axios.defaults.headers.common['Accept'] = 'application/json'
		axios.defaults.headers.common['Request-Origin-App'] = true
	}

	// Load any resources or data that we need prior to rendering the app
	React.useEffect(() => {
		async function loadResourcesAndDataAsync() {
			try {
				SplashScreen.preventAutoHide()

				// Load our initial navigation state
				setInitialNavigationState(await getInitialState())

				// Load fonts
				await Font.loadAsync({
					...Ionicons.font,
					'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')
				})

				await Asset.loadAsync([require('./assets/images/app-splash.png')]),
					// Set default http headers
					setDefaultAxiosHeaders()
			} catch (e) {
				// We might want to provide this error information to an error reporting service
				console.warn(e)
			} finally {
				setLoadingComplete(true)
				SplashScreen.hide()
			}
		}
		loadResourcesAndDataAsync()
	}, [])

	if (!isLoadingComplete && !props.skipLoadingScreen) {
		return null
	} else {
		return (
			<AppContextProvider>
				<View style={styles.container}>
					{Platform.OS === 'ios' && <StatusBar barStyle='default' />}
					{/* <NavigationContainer ref={containerRef} initialState={initialNavigationState}> */}
					<NavigationContainer>
						<Stack.Navigator mode='modal' headerMode='none'>
							<Stack.Screen name='AuthLoading' component={AuthLoadingScreen} />
							{/* <Stack.Screen name='Login' component={LoginScreen} />
							<Stack.Screen name='Register' component={RegisterScreen} /> */}
							{/* <Stack.Screen name='Root' component={BottomTabNavigator} /> */}
						</Stack.Navigator>
					</NavigationContainer>
				</View>
			</AppContextProvider>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff'
	}
})
