import React, { useState } from 'react'
import { AsyncStorage } from 'react-native'
import AppContext from './app-context'
import axios from 'axios'

export default (props) => {
	const [isSignedIn, setIsSignedIn] = useState(false)
	const [isLocationPermissionGranted, setIsLocationPermissionGranted] = useState(null)
	const [accessingUserLocation, setAccessingUserLocation] = useState(false)
	const [user, setUser] = useState(null)
	const [activeJobs, setActiveJobs] = useState([])
	const [upcomingJobs, setUpcomingJobs] = useState([])
	const [jobReceived, setJobReceived] = useState(false)
	const [jobAccepted, setJobAccepted] = useState(false)
	const [currentJobLeg, setCurrentJobLeg] = useState(null) // {type, destination, routeParams: {duration, distance}, entity, isPickup}
	const [isOnline, setIsOnline] = useState(false)
	const [order, setOrder] = useState(null)
	const [dropOffCoords, setDropOffCoords] = useState(null)
	const [pickupCoords, setPickupCoords] = useState(null)
	const [pickupRouteParams, setPickupRouteParams] = useState(null) // {duration, distance, coordinates}
	const [dropOffRouteParams, setdropOffRouteParams] = useState(null) // {duration, distance, coordinates}
	const [refreshJobsList, setRefreshJobsList] = useState(false) // use this as a flag to refresh jobs list

	const [navigationInProgress, setNavigationInProgress] = useState(false)

	const [currentCoords, setCurrentCoords] = useState({
		latitude: 37.78825,
		longitude: -122.4324,
		latitudeDelta: 20,
		longitudeDelta: 20,
	})

	const storeUser = (user) => {
		AsyncStorage.setItem('USER', JSON.stringify(user)).then(() => {
			setUser(user)
			axios.defaults.headers.common['Authorization'] = `Bearer ${user.api_token}`
			// setIsSignedIn(true)
		})
	}

	return (
		<AppContext.Provider
			value={{
				isSignedIn,
				setIsSignedIn,
				user,
				setUser,
				storeUser,
				isOnline,
				setIsOnline,
				isLocationPermissionGranted,
				setIsLocationPermissionGranted,
				accessingUserLocation,
				setAccessingUserLocation,
				currentCoords,
				setCurrentCoords,
				activeJobs,
				setActiveJobs,
				upcomingJobs,
				setUpcomingJobs,
				jobReceived,
				setJobReceived,
				jobAccepted,
				setJobAccepted,
				order,
				setOrder,
				currentJobLeg,
				setCurrentJobLeg,
				dropOffCoords,
				setDropOffCoords,
				pickupCoords,
				setPickupCoords,
				pickupRouteParams,
				setPickupRouteParams,
				dropOffRouteParams,
				setdropOffRouteParams,
				navigationInProgress,
				setNavigationInProgress,
				refreshJobsList,
				setRefreshJobsList,
			}}
		>
			{props.children}
		</AppContext.Provider>
	)
}
