import React from 'react'
export default React.createContext({
	isSignedIn: null,
	setIsSignedIn: null,
	user: null,
	setUser: null,
	isLocationPermissionGranted: null,
	setIsLocationPermissionGranted: null,
	currentCoords: null,
	setCurrentCoords: null,
	jobReceived: null,
	setJobReceived: null,
	order: null,
	setOrder: null,
	dropOffCoords: null,
	setDropOffCoords: null,
	pickupCoords: null,
	setPickupCoords: null,
	currentJobLeg: null,
	setCurrentJobLeg: null
})
