import _ from 'lodash'

export default (place) => {
	let result = {
		country: null,
		city: null,
		state: null,
		zip: null,
		lat: null,
		lng: null,
		line1: null,
	}

	let address_result = _.get(place, 'address_components')

	_.forEach(address_result, function (component) {
		let types = _.get(component, 'types')
		if (_.includes(types, 'country')) {
			result.country = _.get(component, 'long_name')
		}
		if (_.includes(types, 'administrative_area_level_1')) {
			result.state = _.get(component, 'long_name')
		}
		if (_.includes(types, 'locality') || _.includes(types, 'neighborhood')) {
			result.city = _.get(component, 'long_name')
		}
		if (_.includes(types, 'postal_code')) {
			result.zip = _.get(component, 'long_name')
		}
	})

	result.lat = _.get(place, 'geometry.location.lat')
	result.lng = _.get(place, 'geometry.location.lng')
	result.line1 = _.get(place, 'formatted_address')

	return result
}
