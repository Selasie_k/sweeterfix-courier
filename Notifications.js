import React, { useState } from 'react'
import { AsyncStorage, Alert } from 'react-native'
import Pusher from 'pusher-js/react-native'
import AppContext from './context/app-context'
import Config from './config/app'
import axios from 'axios'
import { Notifications } from 'expo'
import * as Permissions from 'expo-permissions'
import * as IntentLauncher from 'expo-intent-launcher'
import { useNavigation } from '@react-navigation/native'
import Layout from './constants/Layout'

export default () => {
	const context = React.useContext(AppContext)
	const navigation = useNavigation()

	React.useEffect(() => {
		_subscribeToPusher(context.user)
		Notifications.addListener(_notificationsHandler)

		registerForPushNotificationsAsync()

		// AsyncStorage.getItem('EXPO_TOKEN').then((token) => {
		// 	console.log(token)
		// 	if (!token) registerForPushNotificationsAsync()
		// })
	}, [])

	const _subscribeToPusher = (user) => {
		const pusher = new Pusher(Config.pusher.APP_KEY, {
			authEndpoint: `${Config.http.baseUrl}/broadcasting/auth`,
			auth: {
				headers: {
					Authorization: `Bearer ${user.api_token}`,
					'Auth-guard': 'courier-api',
				},
			},
			cluster: Config.pusher.APP_CLUSTER,
		})

		const channelName = `private-Courier.${user.id}`

		const userChannel = pusher.subscribe(channelName)

		// const dispatchNotice = pusher.subscribe('dispatch-notice-channel')

		userChannel.bind('Illuminate\\Notifications\\Events\\BroadcastNotificationCreated', (e) => {
			// console.log(e)
			switch (e.type) {
				case 'App\\Notifications\\OrderReadyForDispatch':
					navigation.navigate('DashboardStack', { screen: 'Dashboard' })
					const { order } = e
					context.setOrder(order)
					context.setPickupCoords({ latitude: order.shop.lat, longitude: order.shop.lng })
					context.setDropOffCoords({ latitude: order.shipping.lat, longitude: order.shipping.lng })
					context.setJobReceived(true)

					_showNotification({
						title: 'Dispatch Notice',
						body: `${e.order.shop.name} has an order ready for dispatch`,
					})

					break
			}
		})

		userChannel.bind('App\\Events\\StripeCourierOnboardingCallbackReceived', (e) => {
			context.setUser(e.courier)
			navigation.navigate('SettingsStack', { screen: 'StripeWebView' })
		})
	}

	const _showNotification = async ({ title, body }) => {
		let localNotification = {
			title,
			body,
			ios: {
				sound: true,
				// _displayInForeground: true
			},
			sound: 'default',
		}
		const notif = await Notifications.presentLocalNotificationAsync(localNotification)
		// console.log(notif)
	}

	const registerForPushNotificationsAsync = async () => {
		// const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS)
		// if (status !== 'granted') {
		const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
		if (status !== 'granted') {
			Alert.alert('Failed to get permissions for push notification!')
			return
		}
		// }

		let token = await AsyncStorage.getItem('EXPO_TOKEN')

		if (!token) {
			token = await Notifications.getExpoPushTokenAsync()
		}
		// console.log(token)
		// if (token) {
		sendExpoTokenToServerAsync(token)
		// }

		if (Layout.isAndroid) {
			Notifications.createChannelAndroidAsync('default', {
				name: 'default',
				sound: true,
				priority: 'max',
				vibrate: [0, 250, 250, 250],
			})
		}
	}

	const sendExpoTokenToServerAsync = async (expo_token) => {
		try {
			await axios.post('/courier/exponent/devices/subscribe', { expo_token })
			await AsyncStorage.setItem('EXPO_TOKEN', expo_token)
		} catch (error) {
			console.log(error.response)
		}
	}

	const _notificationsHandler = (notification) => {
		if (Layout.isIOS) Notifications.setBadgeNumberAsync(0)
		if (['selected'].includes(notification.origin)) {
			if (notification.data.type) {
				switch (notification.data.type) {
					case 'ORDER_READY':
						navigation.navigate('JobsStack', { screen: 'AcceptedJob', params: { jobId: notification.data.job.id } })
						break
					case 'NEW_NEARBY_JOB':
						navigation.navigate('JobsStack', { screen: 'ActiveJob', params: { jobId: notification.data.job.id } })
						break

					default:
						break
				}
			}
		}
	}
	return null
}
