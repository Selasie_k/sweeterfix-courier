const baseUrl = 'https://sweeterfix.com/api'
// const baseUrl = 'https://dev.sweeterfix.com/api'
// const baseUrl = 'http://192.168.100.4:80/api'
// const baseUrl = 'http://192.168.8.4:80/api'

export default {
	http: {
		baseUrl,
		baseUrl2: baseUrl.substring(0, baseUrl.length - 4), // Remove the "/api"
	},
	pusher: {
		APP_KEY: '59034df3fb559a019d06',
		APP_CLUSTER: 'eu',
	},
	google: {
		MAPS_APIKEY: 'AIzaSyB2Oaaq4ud1ZEQXowe5N6HGGS7BrmzOWCg',
	},
	version: '1.0.5',
}
