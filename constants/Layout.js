import { Dimensions, Platform, StyleSheet } from 'react-native'
import Colors from './Colors'
import Constants from 'expo-constants'

const { width, height } = Dimensions.get('window')

const elevationShadow = elevation => ({
	elevation,
	shadowColor: 'black',
	shadowOffset: { width: 0, height: 0.5 * elevation },
	shadowOpacity: 0.3,
	shadowRadius: 0.8 * elevation
})

const statusBarHeight = Constants.statusBarHeight

export const Phi = (1 + Math.sqrt(5)) / 2

const fontSize = scale => scale * Phi

export const MIN_HEADER_HEIGHT = 55 + statusBarHeight
export const MAX_HEADER_HEIGHT = height * (1 - 1 / Phi)
export const HEADER_DELTA = MAX_HEADER_HEIGHT - MAX_HEADER_HEIGHT

export const GOOGLE_API_KEY = 'AIzaSyB2Oaaq4ud1ZEQXowe5N6HGGS7BrmzOWCg'

export default {
	window: {
		width,
		height
	},
	styles: {
		elevationShadow,
		mainText: { fontWeight: '400', fontSize: 18, color: Colors.semiMuted },
		// midText: { fontWeight: '400', fontSize: 16, color: Colors.gray[800] },
		subText: { fontWeight: '400', fontSize: 15, marginBottom: 5, color: Colors.muted },
		text: {
			'100': { fontWeight: '500', fontSize: 18, color: Colors.black[800] },
			'200': { fontWeight: '400', fontSize: 16, marginVertical: 3, color: Colors.black[600] },
			'300': { fontWeight: '400', fontSize: 14, marginVertical: 3, color: Colors.black[400] }
		},
		center: { justifyContent: 'center', alignItems: 'center' },
		centerRow: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center' },
		centerBetween: { flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' },
		centerAround: { flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center' },
		cc_icon: { color: Colors.black[600], marginRight: 7 }
	},
	statusBarHeight,
	isSmallDevice: width < 375,
	isIOS: Platform.OS == 'ios',
	isAndroid: Platform.OS == 'android'
}
