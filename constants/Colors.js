const tintColor = '#f57300'

export default {
	tintColor,
	tabIconDefault: '#ccc',
	tabIconSelected: tintColor,
	tabBar: '#fefefe',
	errorBackground: 'red',
	errorText: '#fff',
	warningBackground: '#EAEB5E',
	warningText: '#666804',
	noticeBackground: tintColor,
	noticeText: '#fff',
	orange: tintColor,

	muted: '#a3a3a3',
	// muted: '#a3a3a3',
	semiMuted: '#444444',

	danger: 'red',
	warning: tintColor,
	primary: 'steelblue',
	success: 'green',
	info: 'blue',
	secondary: '#444444',

	listViewBg: '#f4f4f4',

	gray: {
		'100': '#F7FAFC',
		'200': '#EDF2F7',
		'300': '#E2E8F0',
		'400': '#CBD5E0',
		'500': '#A0AEC0',
		'600': '#718096',
		'700': '#4A5568',
		'800': '#2D3748',
		'900': '#1A202C'
	},

	black: {
		'100': '#e3e3e3',
		'200': '#d1d1d1',
		'300': '#bfbfbf',
		'400': '#a1a1a1',
		'500': '#858585',
		'600': '#5c5c5c',
		'700': '#454545',
		'800': '#303030',
		'900': '#1c1c1c'
	},

	orange2: {
		'100': '#FFFAF0',
		'200': '#FEEBC8',
		'300': '#FBD38D',
		'400': '#F6AD55',
		'500': '#ED8936',
		'600': '#DD6B20',
		'700': '#C05621',
		'800': '#9C4221',
		'900': '#7B341E',
		bright: '#ff6347'
	},

	green: {
		'100': '#F0FFF4',
		'200': '#C6F6D5',
		'300': '#9AE6B4',
		'400': '#68D391',
		'500': '#48BB78',
		'600': '#38A169',
		'700': '#2F855A',
		'800': '#276749',
		'900': '#22543D'
	},

	red: {
		'100': '#FFF5F5',
		'200': '#FED7D7',
		'300': '#FEB2B2',
		'400': '#FC8181',
		'500': '#F56565',
		'600': '#E53E3E',
		'700': '#C53030',
		'800': '#9B2C2C',
		'900': '#742A2A'
	}
}
