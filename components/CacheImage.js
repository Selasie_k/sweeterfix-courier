import React, { useEffect, useState } from 'react'
import { Image } from 'react-native'
import shorthash from 'shorthash'
import * as FileSystem from 'expo-file-system'

export default ({ uri, style }) => {
	const [source, setSource] = useState(null)

	useEffect(() => {
		// const { uri } = props
		const name = shorthash.unique(uri)
		const path = `${FileSystem.cacheDirectory}${name}`
		FileSystem.getInfoAsync(path).then((image) => {
			if (image.exists) {
				setSource({ uri: image.uri })
			} else {
				FileSystem.downloadAsync(uri, path).then((newImage) => {
					setSource({ uri: newImage.uri })
				})
			}
		})
	}, [])

	return <Image style={style} source={source} />
}
